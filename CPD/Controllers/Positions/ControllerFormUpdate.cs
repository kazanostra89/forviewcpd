﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Views.Positions;
using CPD.Model;
using CPD.Model.Entities;
using CPD.Model.Tools;

namespace CPD.Controllers.Positions
{
    class ControllerFormUpdate
    {
        private FormUpdate form;
        private DbManager db;
        private Position updatablePosition;

        public ControllerFormUpdate(FormUpdate form)
        {
            this.form = form;
            db = DbManager.GetDbManager();

            updatablePosition = DbHub.HubForEntities as Position;
            DbHub.ResetHubForEntities();

            this.form.statusUpdateDB.Text = String.Empty;
        }

        public void LoadForm()
        {
            if (updatablePosition != null)
            {
                form.textBoxName.Text = updatablePosition.Name;
            }
            else
            {
                form.toolStripButtonUpdate.Enabled = false;
            }
        }

        public bool UpdateSelectedPosition()
        {
            if (form.textBoxName.TextLength == 0)
            {
                form.statusUpdateDB.Text = "Не заполнено поле - Наименование!";
                return false;
            }

            bool resultUpdate = false;

            Position position = new Position()
            {
                PositionID = updatablePosition.PositionID,
                Name = form.textBoxName.Text.Trim()
            };

            try
            {
                db.CheckingConnectionSql();
                
                resultUpdate = db.TablePositions.UpdatePosition(position);

                if (resultUpdate)
                {
                    form.statusUpdateDB.Text = "Выбранная должность успешно изменена!";
                    form.textBoxName.ReadOnly = true;
                    form.toolStripButtonUpdate.Enabled = false;
                    form.toolStripButtonCancel.Text = "Выйти";
                }

            }
            catch (Exception e)
            {
                form.statusUpdateDB.Text = "Ошибка при выполнении операции с БД!";
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return resultUpdate;
        }

        public void CloseForm()
        {
            form.Close();
        }

    }
}
