﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Model;
using CPD.Model.Tools;
using CPD.Model.Entities;
using CPD.Views.Positions;

namespace CPD.Controllers.Positions
{
    class ControllerFormInsert
    {
        private FormInsert form;
        private DbManager db;
        private List<bool> results;

        public ControllerFormInsert(FormInsert form)
        {
            this.form = form;
            db = DbManager.GetDbManager();

            results = new List<bool>();
        }

        public void LoadForm()
        {
            form.toolStripButtonRetry.Enabled = false;
            form.statusInsertDB.Text = String.Empty;
        }

        public void InsertNewPosition()
        {
            if (form.textBoxName.TextLength == 0)
            {
                form.statusInsertDB.Text = "Не заполнено поле - Наименование!";
                return;
            }

            try
            {
                db.CheckingConnectionSql();

                Position position = new Position() { Name = form.textBoxName.Text.Trim() };

                bool result = db.TablePositions.InsertPosition(position);
                results.Add(result);

                if (result)
                {
                    form.toolStripButtonCancel.Text = "Выйти";
                    form.toolStripButtonRetry.Enabled = true;
                    form.toolStripButtonInsert.Enabled = false;
                    form.textBoxName.ReadOnly = true;
                    form.statusInsertDB.Text = "Запись успешно добавлена!";
                }
            }
            catch (Exception e)
            {
                form.statusInsertDB.Text = "Ошибка при выполнении операции с БД!";
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void ToolStripButtonRetryClick()
        {
            form.toolStripButtonRetry.Enabled = false;
            form.toolStripButtonInsert.Enabled = true;
            form.statusInsertDB.Text = String.Empty;
            form.textBoxName.Clear();
            form.textBoxName.ReadOnly = false;
        }

        public void CloseForm()
        {
            form.Close();
        }


        public List<bool> Results
        {
            get { return results; }
        }

    }
}
