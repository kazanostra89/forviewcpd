﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Model;
using CPD.Model.Entities;
using CPD.Model.Tools;
using CPD.Views.Positions;

namespace CPD.Controllers.Positions
{
    class ControllerFormMain
    {
        private FormMain form;
        private DbManager db;
        private Action updatingPhones;

        public ControllerFormMain(FormMain form)
        {
            this.form = form;
            db = DbManager.GetDbManager();
            updatingPhones = DbHub.HubForEntities as Action;
            DbHub.ResetHubForEntities();

            this.form.dataGridViewPositions.RowHeadersVisible = false;
        }

        public void CheckTheInitializationOfTheDelegate()
        {
            if (updatingPhones == null)
            {
                MessageBox.Show("Программная ошибка!", "Инициализация делегата", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                form.Close();
            }
        }

        public void UpdateDataGridViewPositions()
        {
            try
            {
                db.CheckingConnectionSql();

                form.dataGridViewPositions.DataSource = null;
                form.dataGridViewPositions.DataSource = db.TablePositions.GetAllPositions();

                ConfiguringDataDisplay();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void OpenFormInsert()
        {
            using (FormInsert formInsert = new FormInsert())
            {
                formInsert.ShowDialog();

                foreach (bool result in formInsert.ResultsInsert())
                {
                    if (result)
                    {
                        UpdateDataGridViewPositions();
                        break;
                    }
                }
            }
        }

        public void OpenFormUpdate()
        {
            if (form.dataGridViewPositions.SelectedRows.Count == 0)
            {
                MessageBox.Show("Не выбрана должность для изменения!", "Операция изменения", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DbHub.HubForEntities = form.dataGridViewPositions.SelectedRows[0].DataBoundItem;

            using (FormUpdate formUpdate = new FormUpdate())
            {
                formUpdate.UpdatePosition += new Action(UpdateDataGridViewPositions);
                formUpdate.UpdatePosition += updatingPhones;

                formUpdate.ShowDialog();
            }
        }

        public void DeletingTheSelectedPosition()
        {
            if (form.dataGridViewPositions.SelectedRows.Count == 0)
            {
                MessageBox.Show("Не выбрана должность для удаления!", "Операция удаления", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Position selectedPosition = (Position)form.dataGridViewPositions.SelectedRows[0].DataBoundItem;

            try
            {
                db.CheckingConnectionSql();

                bool result = db.TablePositions.DeletePosition(selectedPosition);

                if (result)
                {
                    MessageBox.Show("Должность успешно удалена!");

                    UpdateDataGridViewPositions();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void ConfiguringDataDisplay()
        {
            foreach (DataGridViewColumn item in form.dataGridViewPositions.Columns)
            {
                item.Resizable = DataGridViewTriState.False;
            }

            foreach (DataGridViewRow item in form.dataGridViewPositions.Rows)
            {
                item.Resizable = DataGridViewTriState.False;
                item.MinimumHeight = 30;
            }
           
            form.dataGridViewPositions.Columns["Name"].HeaderText = "Наименование";

            form.dataGridViewPositions.Columns["PositionID"].Visible = false;
                        
            form.dataGridViewPositions.ClearSelection();
        }

        public void FilterForPositions()
        {
            try
            {
                db.CheckingConnectionSql();

                form.dataGridViewPositions.DataSource = null;
                form.dataGridViewPositions.DataSource = db.TablePositions.SearchForPositionsByName(form.toolStripTextBoxFilter.Text);

                ConfiguringDataDisplay();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void ClearTextBoxFilter()
        {
            form.toolStripTextBoxFilter.Clear();
        }

    }
}
