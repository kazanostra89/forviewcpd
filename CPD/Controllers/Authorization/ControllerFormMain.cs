﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Model;
using CPD.Model.Entities;
using CPD.Model.Tools;
using CPD.Views.Authorization;
using CPD.Model.Tools.Exception;

namespace CPD.Controllers.Authorization
{
    class ControllerFormMain
    {
        private FormMain form;
        private DbManager db;

        public ControllerFormMain(FormMain form)
        {
            this.form = form;
            db = DbManager.GetDbManager();
        }

        public void LoadForm()
        {
            form.statusAuthorization.Text = String.Empty;
        }

        public bool AdminAuthentication()
        {
            if (form.textBoxLogin.TextLength == 0)
            {
                form.statusAuthorization.Text = "Не заполнено поле - Логин!";
                return false;
            }

            if (form.textBoxPassword.TextLength == 0)
            {
                form.statusAuthorization.Text = "Не заполнено поле - Пароль!";
                return false;
            }

            bool? result = null;

            Admin admin = new Admin()
            {
                Login = form.textBoxLogin.Text,
                Password = form.textBoxPassword.Text,
                Online = true
            };

            try
            {
                db.ConnectionOpen();

                result = db.TableAdmins.Authentication(admin);
                
                if (result == true)
                {
                    DbHub.HubForEntities = admin;
                    Authentication authentication = Authentication.GetInstance();

                    if (Authentication.Confirmed)
                    {
                        form.toolStripButtonAuthorization.Text = "Вход выполнен";
                        form.toolStripButtonAuthorization.Enabled = false;

                        form.toolStripButtonCancel.Text = "Закрыть";

                        form.textBoxLogin.ReadOnly = true;
                        form.textBoxPassword.ReadOnly = true;
                        form.textBoxPassword.Clear();

                        form.statusAuthorization.Text = "Авторизация упешно выполена! " + admin.Login + " в сети";
                    }
                    else
                    {
                        form.statusAuthorization.Text = "Ошибка при инициализации администратора!";
                        return false;
                    }
                }
                else if (result == null)
                {
                    form.statusAuthorization.Text = "Введен неверный логин!";
                }
                else
                {
                    form.statusAuthorization.Text = "Администратор " + form.textBoxLogin.Text + " уже авторизован!";
                }
            }
            catch(IncorrectPasswordException e)
            {
                form.statusAuthorization.Text = e.Message;
            }
            catch (ErrorProcedureDataBaseException e)
            {
                form.statusAuthorization.Text = e.Message;
            }
            catch (Exception e)
            {
                form.statusAuthorization.Text = "Ошибка при выполнении операции с БД!";
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                if (DbHub.HubForEntities != null) DbHub.ResetHubForEntities();
                db.ConnectionClose();
            }

            return result ?? default(bool);
        }

        public void CloseForm()
        {
            form.Close();
        }

    }
}
