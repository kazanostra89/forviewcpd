﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Model;
using CPD.Model.Tools;
using CPD.Model.Entities;
using CPD.Views.Admins;

namespace CPD.Controllers.Admins
{
    class ControllerFormUpdate
    {
        private FormUpdate form;
        private DbManager db;
        private Admin updatableAdmin;

        public ControllerFormUpdate(FormUpdate form)
        {
            this.form = form;
            db = DbManager.GetDbManager();

            updatableAdmin = DbHub.HubForEntities as Admin;
            DbHub.ResetHubForEntities();
            
            this.form.statusUpdateDB.Text = String.Empty;
        }

        public void LoadForm()
        {
            if (updatableAdmin != null)
            {
                form.Text = "Редактируется: " + updatableAdmin.Login + new String(' ', 2) + updatableAdmin.AdminID;
                form.radioButtonLoginPassword.Checked = true;
            }
            else
            {
                form.Text = "Программная ошибка";
                form.toolStripButtonUpdate.Enabled = false;
            }
        }

        public void RadioButtonCheckedChanged(object sender)
        {
            if (updatableAdmin == null)
            {
                return;
            }

            form.textBoxLogin.Clear();
            form.textBoxPassword.Clear();

            if (sender is RadioButton)
            {
                RadioButton radioButton = (RadioButton)sender;

                if (radioButton.Checked)
                {
                    switch (radioButton.Text)
                    {
                        case "Логин":
                            form.textBoxLogin.ReadOnly = false;
                            form.textBoxPassword.ReadOnly = true;
                            break;
                        case "Пароль":
                            form.textBoxLogin.Text = updatableAdmin.Login;
                            form.textBoxLogin.ReadOnly = true;
                            form.textBoxPassword.ReadOnly = false;
                            break;
                        case "Логин и пароль":
                            form.textBoxLogin.ReadOnly = false;
                            form.textBoxPassword.ReadOnly = false;
                            break;
                    }
                }
            }
        }

        public bool UpdateSelectedAdmin(Control.ControlCollection radioButtons)
        {
            Admin admin = new Admin() { AdminID = updatableAdmin.AdminID };
            bool resultUpdate = false;

            foreach (Control item in radioButtons)
            {
                if (item is RadioButton)
                {
                    RadioButton radioButton = (RadioButton)item;

                    if (radioButton.Checked)
                    {
                        switch (radioButton.Text)
                        {
                            case "Логин":
                                if (form.textBoxLogin.TextLength == 0)
                                {
                                    form.statusUpdateDB.Text = "Не заполнено поле - Логин!";
                                    return false;
                                }
                                else if (updatableAdmin.Login.ToUpper() == "SYSDBA")
                                {
                                    form.statusUpdateDB.Text = "Изменение логина - SYSDBA запрещено!";
                                    return false;
                                }

                                admin.Login = form.textBoxLogin.Text;
                                admin.Password = null;
                                break;
                            case "Пароль":
                                if (form.textBoxPassword.TextLength == 0)
                                {
                                    form.statusUpdateDB.Text = "Не заполнено поле - Пароль!";
                                    return false;
                                }

                                admin.Login = null;
                                admin.Password = form.textBoxPassword.Text;
                                break;
                            case "Логин и пароль":
                                if (form.textBoxLogin.TextLength == 0)
                                {
                                    form.statusUpdateDB.Text = "Не заполнено поле - Логин!";
                                    return false;
                                }
                                else if (updatableAdmin.Login.ToUpper() == "SYSDBA")
                                {
                                    form.statusUpdateDB.Text = "Изменение логина - SYSDBA запрещено!";
                                    return false;
                                }

                                if (form.textBoxPassword.TextLength == 0)
                                {
                                    form.statusUpdateDB.Text = "Не заполнено поле - Пароль!";
                                    return false;
                                }

                                admin.Login = form.textBoxLogin.Text;
                                admin.Password = form.textBoxPassword.Text;
                                break;
                        }
                    }
                }
            }

            try
            {
                db.CheckingConnectionSql();

                resultUpdate = db.TableAdmins.UpdateAdminByGUID(admin);

                if (resultUpdate)
                {
                    form.toolStripButtonUpdate.Enabled = false;
                    form.toolStripButtonCancel.Text = "Выйти";
                    form.statusUpdateDB.Text = "Данные администратора " + updatableAdmin.AdminID + " обновлены!";

                    foreach (Control item in radioButtons)
                    {
                        if (item is TextBox) ((TextBox)item).ReadOnly = true;

                        if (item is RadioButton) ((RadioButton)item).Enabled = false;
                    }
                }
            }
            catch (Exception e)
            {
                form.statusUpdateDB.Text = "Ошибка при выполнении операции с БД!";
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return resultUpdate;
        }

        public void CloseForm()
        {
            form.Close();
        }

    }
}
