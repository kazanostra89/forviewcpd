﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Model;
using CPD.Model.Tools;
using CPD.Model.Entities;
using CPD.Views.Admins;

namespace CPD.Controllers.Admins
{
    class ControllerFormInsert
    {
        private FormInsert form;
        private DbManager db;
        private List<bool> results;

        public ControllerFormInsert(FormInsert form)
        {
            this.form = form;
            db = DbManager.GetDbManager();

            results = new List<bool>();
        }

        public void LoadForm()
        {
            form.toolStripButtonRetry.Enabled = false;
            form.statusInsertDB.Text = String.Empty;
        }

        public void InsertNewAdmin()
        {
            if (form.textBoxLogin.TextLength == 0)
            {
                form.statusInsertDB.Text = "Не заполнено поле - Логин!";
                return;
            }

            if (form.textBoxPassword.TextLength == 0)
            {
                form.statusInsertDB.Text = "Не заполнено поле - Пароль!";
                return;
            }

            try
            {
                db.CheckingConnectionSql();

                Admin admin = new Admin() { Login = form.textBoxLogin.Text, Password = form.textBoxPassword.Text };

                bool result = db.TableAdmins.InsertAdmin(admin);
                results.Add(result);
                
                if (result && DbHub.HubForEntities is Guid)
                {
                    Guid adminID = (Guid)DbHub.HubForEntities;
                    DbHub.ResetHubForEntities();

                    form.toolStripButtonCancel.Text = "Выйти";
                    form.toolStripButtonRetry.Enabled = true;
                    form.toolStripButtonInsert.Enabled = false;
                    form.textBoxLogin.ReadOnly = true;
                    form.textBoxPassword.ReadOnly = true;
                    form.statusInsertDB.Text = "Запись добавлена!" + " AdminID: " + adminID.ToString();
                }
            }
            catch (Exception e)
            {
                form.statusInsertDB.Text = "Ошибка при выполнении операции с БД!";
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void ToolStripButtonRetryClick()
        {
            form.toolStripButtonRetry.Enabled = false;
            form.toolStripButtonInsert.Enabled = true;
            form.statusInsertDB.Text = String.Empty;
            form.textBoxLogin.Clear();
            form.textBoxPassword.Clear();
            form.textBoxLogin.ReadOnly = false;
            form.textBoxPassword.ReadOnly = false;
        }

        public void CloseForm()
        {
            form.Close();
        }
        

        public List<bool> Results
        {
            get { return results; }
        }
        
    }
}
