﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPD.Views.Admins;
using CPD.Model;
using CPD.Model.Entities;
using CPD.Model.Tools;
using System.Windows.Forms;

namespace CPD.Controllers.Admins
{
    class ControllerFormMain
    {
        private FormMain form;
        private DbManager db;

        public ControllerFormMain(FormMain form)
        {
            this.form = form;
            db = DbManager.GetDbManager();

            this.form.dataGridViewAdmins.RowHeadersVisible = false;
        }

        public void UpdateDataGridViewAdmins()
        {
            try
            {
                db.CheckingConnectionSql();

                form.dataGridViewAdmins.DataSource = null;
                form.dataGridViewAdmins.DataSource = db.TableAdmins.GetAllAdmins();

                ConfiguringDataDisplay();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            #region Testing code
            //form.dataGridViewAdmins.SelectedRows[0].Cells["Online"].Value

            /*foreach (DataGridViewRow item in dataGridViewAdmins.Rows)
            {
                item.Height = 30;
                item.DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomLeft;
            }

            for (int i =0; i < dataGridViewAdmins.Rows.Count; i++)
            {
                dataGridViewAdmins.Rows[i].HeaderCell.Value = (i + 1).ToString();
            }

            dataGridViewAdmins.RowHeadersDefaultCellStyle.Padding = new Padding(0, 7, 0, 7);
            dataGridViewAdmins.RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomLeft;*/
            #endregion
        }

        public void OpenFormInsert()
        {
            using (FormInsert form = new FormInsert())
            {
                form.ShowDialog();

                foreach (bool result in form.ResultsInsert())
                {
                    if (result)
                    {
                        UpdateDataGridViewAdmins();
                        break;
                    }
                }
            }
        }

        public void OpenFormUpdate()
        {
            if (form.dataGridViewAdmins.SelectedRows.Count == 0)
            {
                MessageBox.Show("Не выбран администратор для изменения!", "Операция изменения", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DbHub.HubForEntities = form.dataGridViewAdmins.SelectedRows[0].DataBoundItem;

            using (FormUpdate form = new FormUpdate())
            {
                form.UpdateAdmin += new Action(UpdateDataGridViewAdmins);

                form.ShowDialog();
            }
        }

        public void DeletingTheSelectedAdministrator()
        {
            if (form.dataGridViewAdmins.SelectedRows.Count == 0)
            {
                MessageBox.Show("Не выбран администратор для удаления!", "Операция удаления", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Admin selectedAdmin = (Admin)form.dataGridViewAdmins.SelectedRows[0].DataBoundItem;

            if (selectedAdmin.Login.ToUpper() == "SYSDBA")
            {
                MessageBox.Show("Удаление администратора \"SYSDBA\" невозможно!", "Операция удаления", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
                        
            try
            {
                db.CheckingConnectionSql();

                bool result = db.TableAdmins.DeleteAdminByGUID(selectedAdmin);

                if (result)
                {
                    MessageBox.Show("Администратор " + selectedAdmin.AdminID +  " успешно удален!", "Операция удаления", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    UpdateDataGridViewAdmins();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void ChangingTheOnlineStatus()
        {
            if (form.dataGridViewAdmins.SelectedRows.Count == 0)
            {
                MessageBox.Show("Не выбран администратор для изменения!", "Операция изменения", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Admin selectedAdmin = (Admin)form.dataGridViewAdmins.SelectedRows[0].DataBoundItem;

            if (selectedAdmin.Login.ToUpper() == "SYSDBA")
            {
                MessageBox.Show("Изменение администратора \"SYSDBA\" невозможно!", "Операция изменения", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            selectedAdmin.Online = selectedAdmin.Online ? false : true;

            try
            {
                db.CheckingConnectionSql();

                bool result = db.TableAdmins.UpdateOnlineStatus(selectedAdmin);
                
                if (result)
                {
                    MessageBox.Show("Администратор " + selectedAdmin.AdminID + " успешно изменен!", "Операция изменения", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    UpdateDataGridViewAdmins();
                }
                else
                {
                    MessageBox.Show("Запрос на изменение в БД не был выполнен\nПовторите операцию!", "Операция изменения", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    selectedAdmin.Online = selectedAdmin.Online ? false : true;
                }
            }
            catch (Exception e)
            {
                selectedAdmin.Online = selectedAdmin.Online ? false : true;
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void ConfiguringDataDisplay()
        {
            foreach (DataGridViewColumn item in form.dataGridViewAdmins.Columns)
            {
                item.Resizable = DataGridViewTriState.False;
            }

            foreach (DataGridViewRow item in form.dataGridViewAdmins.Rows)
            {
                item.Resizable = DataGridViewTriState.False;
                item.MinimumHeight = 30;
            }

            form.dataGridViewAdmins.Columns["AdminID"].HeaderText = "Идентификатор";
            form.dataGridViewAdmins.Columns["Login"].HeaderText = "Логин";
            form.dataGridViewAdmins.Columns["Online"].HeaderText = "Состояние";

            form.dataGridViewAdmins.Columns["Password"].Visible = false;

            form.dataGridViewAdmins.Columns["Login"].DisplayIndex = 0;
            form.dataGridViewAdmins.Columns["Online"].DisplayIndex = 2;
            form.dataGridViewAdmins.Columns["AdminID"].DisplayIndex = 1;

            form.dataGridViewAdmins.Columns["Login"].MinimumWidth = 100;
            form.dataGridViewAdmins.Columns["Online"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            form.dataGridViewAdmins.Columns["AdminID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            form.dataGridViewAdmins.Columns["Online"].ContextMenuStrip = form.contextMenuColumnOnline;

            form.dataGridViewAdmins.ClearSelection();
        }

        public void FilterForAdmins()
        {
            string searchAdmin = form.toolStripTextBoxFilter.Text;

            try
            {
                db.CheckingConnectionSql();

                form.dataGridViewAdmins.DataSource = null;
                form.dataGridViewAdmins.DataSource = db.TableAdmins.SearchForAdminsByLogin(searchAdmin);

                ConfiguringDataDisplay();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void ClearTextBoxFilter()
        {
            form.toolStripTextBoxFilter.Clear();
        }


    }
}
