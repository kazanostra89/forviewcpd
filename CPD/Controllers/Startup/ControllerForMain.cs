﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPD.Views.Startup;
using CPD.Model;
using CPD.Model.Tools;
using CPD.Model.Entities;
using System.Windows.Forms;
using System.Threading;
using System.Drawing;

namespace CPD.Controllers.Startup
{
    class ControllerForMain
    {
        private FormMain form;
        private DbManager db;

        public ControllerForMain(FormMain form)
        {
            this.form = form;

            try
            {
                db = DbManager.GetDbManager();

                TheInitialDisplayOfTheForm();
            }
            catch (Exception e)
            {
                string message = "Критическая ошибка приложения!\nПрограмма будет закрыта. SQL Error:\n";

                MessageBox.Show(message + e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Application.Exit();
            }
        }

        public void TheInitialDisplayOfTheForm()
        {
            form.buttonAuthentication.Text = "Авторизоваться";
            form.buttonAuthentication.Enabled = true;
            form.statusAuthorization.Text = String.Empty;
            form.buttonAuthenticationQuit.Visible = false;
            form.buttonEditingPhones.Visible = false;
            form.buttonOpenConsoleAdmin.Visible = false;
            form.Size = new Size(355, 175);
        }

        public void DisplayOfTheForm()
        {
            if (Authentication.Confirmed && Authentication.GetInstance().CurrentAdmin != null)
            {
                Admin currentAdmmin = Authentication.GetInstance().CurrentAdmin;

                form.buttonAuthentication.Text = currentAdmmin.Login.ToUpper();
                form.statusAuthorization.Text = currentAdmmin.AdminID.ToString().ToUpper();
                form.buttonAuthentication.Enabled = false;
                form.buttonAuthenticationQuit.Visible = true;

                if (currentAdmmin.Login.ToUpper() == "SYSDBA")
                {
                    form.Size = new Size(355, 310);
                    form.buttonOpenConsoleAdmin.Visible = true;
                }
                else
                {
                    form.Size = new Size(355, 240);
                }

                form.buttonEditingPhones.Visible = true;
            }
        }

        public void OpenFormAdmins()
        {
            try
            {
                db.ConnectionOpen();

                using (Views.Admins.FormMain formMain = new Views.Admins.FormMain())
                {
                    formMain.ShowDialog();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                db.ConnectionClose();
            }
        }

        public void OpenFormAuthorization()
        {
            using (Views.Authorization.FormMain authorizationForm = new Views.Authorization.FormMain())
            {
                authorizationForm.ResultAuthentication += new Action(DisplayOfTheForm);

                authorizationForm.ShowDialog();
            }
        }

        public void OpenFastDataViewingForm()
        {
            using (Views.FastDataViewing.FormMain formMain = new Views.FastDataViewing.FormMain())
            {
                formMain.ShowDialog();
            }
        }

        public void OpenPhonesForm()
        {
            try
            {
                db.ConnectionOpen();

                using (Views.Phones.FormMain formMain = new Views.Phones.FormMain())
                {
                    formMain.ShowDialog();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                db.ConnectionClose();
            }
        }

        public void Quit()
        {
            if (Authentication.Confirmed)
            {
                Authentication currentAuthentication = Authentication.GetInstance();
                Admin currentAdmin = null;

                try
                {
                    db.ConnectionOpen();

                    currentAdmin = currentAuthentication.CurrentAdmin;
                    currentAdmin.Online = false;

                    bool result = db.TableAdmins.UpdateOnlineStatus(currentAdmin);

                    if (result)
                    {
                        currentAuthentication.Quit();
                        
                        TheInitialDisplayOfTheForm();

                        MessageBox.Show(currentAdmin.Login.ToUpper() + " успешно вышел!", "Выход из БД", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        currentAdmin.Online = true;
                        MessageBox.Show("Вы еще в сети повторите попытку!", "Выход из БД", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception e)
                {
                    if (currentAdmin.Online == false) currentAdmin.Online = true;

                    MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                finally
                {
                    db.ConnectionClose();
                }
            }
        }

    }
}
