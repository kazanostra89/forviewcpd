﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Views.Departments;
using CPD.Model;
using CPD.Model.Entities;
using CPD.Model.Tools;

namespace CPD.Controllers.Departments
{
    class ControllerFormUpdate
    {
        private FormUpdate form;
        private DbManager db;
        private Department updatableDepartment;

        public ControllerFormUpdate(FormUpdate form)
        {
            this.form = form;
            db = DbManager.GetDbManager();

            updatableDepartment = DbHub.HubForEntities as Department;
            DbHub.ResetHubForEntities();

            this.form.statusUpdateDB.Text = String.Empty;
        }

        public void LoadForm()
        {
            if (updatableDepartment != null)
            {
                form.textBoxName.Text = updatableDepartment.Name;
            }
            else
            {
                form.toolStripButtonUpdate.Enabled = false;
            }
        }

        public bool UpdateSelectedDepartment()
        {
            if (form.textBoxName.TextLength == 0)
            {
                form.statusUpdateDB.Text = "Не заполнено поле - Наименование!";
                return false;
            }

            bool resultUpdate = false;
            Department department = new Department()
            {
                DepartmentID = updatableDepartment.DepartmentID,
                Name = form.textBoxName.Text.Trim()
            };

            try
            {
                db.CheckingConnectionSql();
                
                resultUpdate = db.TableDepartments.UpdateDepartment(department);

                if (resultUpdate)
                {
                    form.statusUpdateDB.Text = "Выбранное подразделение успешно изменено!";
                    form.textBoxName.ReadOnly = true;
                    form.toolStripButtonUpdate.Enabled = false;
                    form.toolStripButtonCancel.Text = "Выйти";
                }

            }
            catch (Exception e)
            {
                form.statusUpdateDB.Text = "Ошибка при выполнении операции с БД!";
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return resultUpdate;
        }

        public void CloseForm()
        {
            form.Close();
        }

    }
}
