﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Model;
using CPD.Model.Entities;
using CPD.Model.Tools;
using CPD.Views.Departments;

namespace CPD.Controllers.Departments
{
    class ControllerFormMain
    {
        private FormMain form;
        private DbManager db;
        private Action updatingPhones;

        public ControllerFormMain(FormMain form)
        {
            this.form = form;
            db = DbManager.GetDbManager();
            updatingPhones = DbHub.HubForEntities as Action;
            DbHub.ResetHubForEntities();

            this.form.dataGridViewDepartments.RowHeadersVisible = false;
        }

        public void CheckTheInitializationOfTheDelegate()
        {
            if (updatingPhones == null)
            {
                MessageBox.Show("Программная ошибка!", "Инициализация делегата", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                form.Close();
            }
        }

        public void UpdateDataGridViewDepartments()
        {
            try
            {
                db.CheckingConnectionSql();

                form.dataGridViewDepartments.DataSource = null;
                form.dataGridViewDepartments.DataSource = db.TableDepartments.GetAllDepartments();

                ConfiguringDataDisplay();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "MSSQL Server", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void OpenFormInsert()
        {
            using (FormInsert formInsert = new FormInsert())
            {
                formInsert.ShowDialog();

                foreach (bool result in formInsert.ResultsInsert())
                {
                    if (result)
                    {
                        UpdateDataGridViewDepartments();
                        break;
                    }
                }
            }
        }

        public void OpenFormUpdate()
        {
            if (form.dataGridViewDepartments.SelectedRows.Count == 0)
            {
                MessageBox.Show("Не выбрано подразделение для изменения!", "Операция изменения", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DbHub.HubForEntities = form.dataGridViewDepartments.SelectedRows[0].DataBoundItem;

            using (FormUpdate formUpdate = new FormUpdate())
            {
                formUpdate.UpdateDepartment += new Action(UpdateDataGridViewDepartments);
                formUpdate.UpdateDepartment += updatingPhones;
                
                formUpdate.ShowDialog();
            }
        }

        public void DeletingTheSelectedDepartment()
        {
            if (form.dataGridViewDepartments.SelectedRows.Count == 0)
            {
                MessageBox.Show("Не выбрано подразделение для удаления!", "Операция удаления", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Department selectedDepartment = (Department)form.dataGridViewDepartments.SelectedRows[0].DataBoundItem;

            try
            {
                db.CheckingConnectionSql();

                bool result = db.TableDepartments.DeleteDepartment(selectedDepartment);

                if (result)
                {
                    MessageBox.Show("Подразделение успешно удалено!");

                    UpdateDataGridViewDepartments();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void ConfiguringDataDisplay()
        {
            foreach (DataGridViewColumn item in form.dataGridViewDepartments.Columns)
            {
                item.Resizable = DataGridViewTriState.False;
            }

            foreach (DataGridViewRow item in form.dataGridViewDepartments.Rows)
            {
                item.Resizable = DataGridViewTriState.False;
                item.MinimumHeight = 30;
            }
           
            form.dataGridViewDepartments.Columns["Name"].HeaderText = "Наименование";

            form.dataGridViewDepartments.Columns["DepartmentID"].Visible = false;
                        
            form.dataGridViewDepartments.ClearSelection();
        }

        public void FilterForDepartments()
        {
            try
            {
                db.CheckingConnectionSql();

                form.dataGridViewDepartments.DataSource = null;
                form.dataGridViewDepartments.DataSource = db.TableDepartments.SearchForDepartmentsByName(form.toolStripTextBoxFilter.Text);

                ConfiguringDataDisplay();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void ClearTextBoxFilter()
        {
            form.toolStripTextBoxFilter.Clear();
        }

    }
}
