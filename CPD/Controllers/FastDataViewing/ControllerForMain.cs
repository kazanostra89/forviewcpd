﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Model;
using CPD.Model.Entities;
using CPD.Views.FastDataViewing;

namespace CPD.Controllers.FastDataViewing
{
    class ControllerForMain
    {
        private FormMain form;
        private DbManager db;
        private List<PhoneView> phones;

        private StringBuilder request;
        private string currentSearchColumn;

        public ControllerForMain(FormMain form)
        {
            this.form = form;
            db = DbManager.GetDbManager();
            phones = db.ViewPhones.Rows;

            request = new StringBuilder();
            currentSearchColumn = "FullName";

            this.form.dataGridViewPhones.RowHeadersVisible = false;
        }

        public void LoadForm()
        {
            if (phones.Count != 0)
            {
                LoadingInitialDataSource();
            }
            else
            {
                try
                {
                    db.ConnectionOpen();

                    LoadPhonesFromDB();
                }
                catch (Exception e)
                {
                    string message = "Ошибка при выполнении операции с БД!\nОкно будет закрыто. SQL Error:\n";

                    MessageBox.Show(message + e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    form.Close();
                }
                finally
                {
                    db.ConnectionClose();
                }
            }

            form.labelRequestSearch.Text = string.Empty;
            form.labelCurrentColumn.Text = NameForTheColumn();

            ComboBoxInitialization();
        }

        public void UpdateDataGridViewPhones()
        {
            try
            {
                db.ConnectionOpen();

                LoadPhonesFromDB();

                ComboBoxInitialization();
            }
            catch (Exception e)
            {
                //Возможно только когда мы применяли фильтры, но при запросе из БД произошла ошибка!
                if (!phones.Equals(db.ViewPhones.Rows))
                {
                    phones = db.ViewPhones.Rows;
                }

                string message = "Ошибка обновления данных из БД!\nSQL Error:\n";

                MessageBox.Show(message + e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                db.ConnectionClose();
            }
        }

        private void LoadPhonesFromDB()
        {
            form.dataGridViewPhones.DataSource = null;

            db.ViewPhones.GetAllPhones();

            if (!phones.Equals(db.ViewPhones.Rows))
            {
                phones = db.ViewPhones.Rows;
            }

            form.dataGridViewPhones.DataSource = phones;

            ConfiguringDataDisplay();
        }

        private void LoadingInitialDataSource()
        {
            form.dataGridViewPhones.DataSource = null;
            form.dataGridViewPhones.DataSource = phones;
            ConfiguringDataDisplay();
        }

        private void ConfiguringDataDisplay()
        {
            foreach (DataGridViewColumn item in form.dataGridViewPhones.Columns)
            {
                item.Resizable = DataGridViewTriState.False;
            }

            foreach (DataGridViewRow item in form.dataGridViewPhones.Rows)
            {
                item.Resizable = DataGridViewTriState.False;
                item.MinimumHeight = 20;
            }

            form.dataGridViewPhones.Columns["FullName"].HeaderText = "ФИО";
            form.dataGridViewPhones.Columns["NumberCity"].HeaderText = "Внутренний";
            form.dataGridViewPhones.Columns["NumberMobile"].HeaderText = "Корпоративный";
            form.dataGridViewPhones.Columns["Position"].HeaderText = "Должность";
            form.dataGridViewPhones.Columns["Department"].HeaderText = "Подразделение";

            form.dataGridViewPhones.Columns["FullName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            form.dataGridViewPhones.Columns["NumberCity"].MinimumWidth = 100;
            form.dataGridViewPhones.Columns["NumberMobile"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            form.dataGridViewPhones.Columns["Position"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            form.dataGridViewPhones.Columns["Department"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            
            form.dataGridViewPhones.ClearSelection();
        }

        private void ComboBoxInitialization()
        {
            ComboBoxClearning();

            form.ComboBoxPosition.Items.AddRange(phones.ConvertAll<object>(item => item.Position).Distinct().ToArray());
            form.ComboBoxDepartment.Items.AddRange(phones.ConvertAll<object>(item => item.Department).Distinct().ToArray());

            form.ComboBoxPosition.Sorted = true;
            form.ComboBoxDepartment.Sorted = true;
        }

        private void ComboBoxClearning()
        {
            form.ComboBoxPosition.Text = String.Empty;
            form.ComboBoxPosition.Items.Clear();

            form.ComboBoxDepartment.Text = String.Empty;
            form.ComboBoxDepartment.Items.Clear();
        }

        public void SmartSearch(KeyPressEventArgs e)
        {
            if (e.KeyChar == '/' || request.ToString().StartsWith("/"))
            {
                if (form.labelStatusSearch.Text != "ВКЛ")
                {
                    form.labelStatusSearch.Text = "ВКЛ";
                }

                if (form.dataGridViewPhones.SelectedCells.Count != 0 && request.Length == 0)
                {
                    currentSearchColumn = form.dataGridViewPhones.SelectedCells[0].OwningColumn.Name;
                    form.labelCurrentColumn.Text = NameForTheColumn();
                }

                if (e.KeyChar == '/' && request.ToString().StartsWith("/"))
                {
                    e.Handled = true;
                    return;
                }

                if (e.KeyChar == (char)Keys.Back)
                {
                    if (request.Length > 1)
                    {
                        request.Remove(request.Length - 1, 1);
                        form.labelRequestSearch.Text = request.ToString();

                        SearchBySelectedColumn();
                    }

                    return;
                }

                if (e.KeyChar == (char)Keys.Escape)
                {
                    request.Clear();
                    form.labelRequestSearch.Text = String.Empty;
                    currentSearchColumn = "FullName";
                    form.labelCurrentColumn.Text = NameForTheColumn();
                    form.labelStatusSearch.Text = "ВЫКЛ";
                    LoadingInitialDataSource();

                    e.Handled = true;
                    return;
                }

                form.labelRequestSearch.Text = request.Append(e.KeyChar).ToString();

                if (request.Length == 1) return;

                SearchBySelectedColumn();
            }
        }

        private void SearchBySelectedColumn()
        {
            form.dataGridViewPhones.DataSource = null;

            switch (currentSearchColumn)
            {
                case "FullName":
                    form.dataGridViewPhones.DataSource = phones.Where(item => item.FullName.ToUpper().Contains(request.ToString().ToUpper().Replace("/", String.Empty))).ToList();
                    break;
                case "NumberCity":
                    if (request.ToString().Replace("/", String.Empty) == String.Empty)
                    {
                        LoadingInitialDataSource();
                        return;
                    }

                    form.dataGridViewPhones.DataSource = phones.Where(item => item.NumberCity != null && item.NumberCity.StartsWith(request.ToString().Replace("/", String.Empty))).ToList();
                    break;
                case "NumberMobile":
                    if (request.ToString().Replace("/", String.Empty) == String.Empty)
                    {
                        LoadingInitialDataSource();
                        return;
                    }

                    form.dataGridViewPhones.DataSource = phones.Where(item => item.NumberMobile != null && item.NumberMobile.Contains(request.ToString().Replace("/", String.Empty))).ToList();
                    break;
                case "Position":
                    form.dataGridViewPhones.DataSource = phones.Where(item => item.Position.ToUpper().Contains(request.ToString().ToUpper().Replace("/", String.Empty))).ToList();
                    break;
                case "Department":
                    form.dataGridViewPhones.DataSource = phones.Where(item => item.Department.ToUpper().Contains(request.ToString().ToUpper().Replace("/", String.Empty))).ToList();
                    break;
            }

            ConfiguringDataDisplay();
        }

        private string NameForTheColumn()
        {
            switch (currentSearchColumn)
            {
                case "NumberCity":
                    return "Внутренний номер";
                case "NumberMobile":
                    return "Корпоративный номер";
                case "Position":
                    return "Должность";
                case "Department":
                    return "Подразделение";
                default:
                    return "ФИО";
            }
        }

        public void Filter()
        {
            string department = form.ComboBoxDepartment.Text;
            string position = form.ComboBoxPosition.Text;

            if (position.Length == 0 && department.Length != 0)
            {
                phones = db.ViewPhones.Rows.Where(item => item.Department == form.ComboBoxDepartment.Text).ToList();
            }
            else if (position.Length != 0 && department.Length == 0)
            {
                phones = db.ViewPhones.Rows.Where(item => item.Position == form.ComboBoxPosition.Text).ToList();
            }
            else if (position.Length != 0 && department.Length != 0)
            {
                phones = db.ViewPhones.Rows.Where(item => item.Position == form.ComboBoxPosition.Text && item.Department == form.ComboBoxDepartment.Text).ToList();
            }
            else
            {
                if (phones.Equals(db.ViewPhones.Rows))
                {
                    return;
                }

                phones = db.ViewPhones.Rows;
            }

            LoadingInitialDataSource();
        }

        public void ResetTheFilter()
        {
            form.ComboBoxPosition.Text = String.Empty;
            form.ComboBoxDepartment.Text = String.Empty;

            if (phones.Equals(db.ViewPhones.Rows))
            {
                return;
            }

            phones = db.ViewPhones.Rows;
            LoadingInitialDataSource();
        }

    }
}
