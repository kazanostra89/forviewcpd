﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Model;
using CPD.Model.Tools;
using CPD.Model.Entities;
using CPD.Views.Phones;

namespace CPD.Controllers.Phones
{
    class ControllerFormInsert
    {
        private FormInsert form;
        private DbManager db;
        private List<bool> results;

        public ControllerFormInsert(FormInsert form)
        {
            this.form = form;
            db = DbManager.GetDbManager();

            results = new List<bool>();
        }

        public void LoadForm()
        {
            form.toolStripButtonRetry.Enabled = false;
            form.statusInsertDB.Text = String.Empty;

            try
            {
                form.comboBoxDepartments.DataSource = db.TableDepartments.GetAllDepartments();
                form.comboBoxPositions.DataSource = db.TablePositions.GetAllPositions();

                if (form.comboBoxDepartments.Items.Count == 0 || form.comboBoxPositions.Items.Count == 0)
                {
                    MessageBox.Show("Справочники пусты!\nОперация невозможна", "Исключительная ситуация", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    form.Close();
                }
            }
            catch (Exception e)
            {
                string message = "Ошибка при выполнении операции с БД!\nОкно будет закрыто. SQL Error:\n";

                MessageBox.Show(message + e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                form.Close();
            }
        }

        public void CheckBoxCheckedChanged(object sender)
        {
            if (sender is CheckBox)
            {
                CheckBox checkBox = (CheckBox)sender;

                switch (checkBox.Name)
                {
                    case "checkBoxNumberCity":
                        {
                            if (checkBox.Checked)
                            {
                                form.maskedTextBoxNumberCity.Enabled = true;
                            }
                            else
                            {
                                form.maskedTextBoxNumberCity.Enabled = false;
                            }

                            form.maskedTextBoxNumberCity.Clear();
                        }
                        break;
                    default:
                        {
                            if (checkBox.Checked)
                            {
                                form.maskedTextBoxNumberMobile.Enabled = true;
                            }
                            else
                            {
                                form.maskedTextBoxNumberMobile.Enabled = false;
                            }

                            form.maskedTextBoxNumberMobile.Clear();
                        }
                        break;
                }
            }
        }

        public void InsertNewPhone()
        {
            if (!form.checkBoxNumberCity.Checked && !form.checkBoxNumberMobile.Checked)
            {
                form.statusInsertDB.Text = "Один из телефонных номеров должен быть заполнен!";
                return;
            }

            if (form.textBoxFullName.TextLength == 0)
            {
                form.statusInsertDB.Text = "Не заполнено поле - ФИО!";
                return;
            }

            Phone phone = new Phone();

            if (form.checkBoxNumberCity.CheckState == CheckState.Checked && form.checkBoxNumberMobile.CheckState == CheckState.Unchecked)
            {
                if (form.maskedTextBoxNumberCity.Text.Contains(" ") || form.maskedTextBoxNumberCity.TextLength != 5)
                {
                    form.statusInsertDB.Text = "Не заполнено поле - Внутренний номер!";
                    return;
                }

                phone.NumberCity = Convert.ToInt32(form.maskedTextBoxNumberCity.Text.Remove(2, 1));
            }
            else if (form.checkBoxNumberCity.CheckState == CheckState.Unchecked && form.checkBoxNumberMobile.CheckState == CheckState.Checked)
            {
                if (form.maskedTextBoxNumberMobile.Text.Contains(" ") || form.maskedTextBoxNumberMobile.TextLength != 16)
                {
                    form.statusInsertDB.Text = "Не заполнено поле - Корпоративный номер!";
                    return;
                }
                
                phone.NumberMobile = form.maskedTextBoxNumberMobile.Text;
            }
            else
            {
                if (form.maskedTextBoxNumberCity.Text.Contains(" ") || form.maskedTextBoxNumberCity.TextLength != 5)
                {
                    form.statusInsertDB.Text = "Не заполнено поле - Внутренний номер!";
                    return;
                }

                if (form.maskedTextBoxNumberMobile.Text.Contains(" ") || form.maskedTextBoxNumberMobile.TextLength != 16)
                {
                    form.statusInsertDB.Text = "Не заполнено поле - Корпоративный номер!";
                    return;
                }

                phone.NumberCity = Convert.ToInt32(form.maskedTextBoxNumberCity.Text.Remove(2, 1));
                phone.NumberMobile = form.maskedTextBoxNumberMobile.Text;
            }

            phone.FullName = form.textBoxFullName.Text.Trim(' ');
            phone.DepartmentID = ((Department)form.comboBoxDepartments.SelectedItem).DepartmentID;
            phone.PositionID = ((Position)form.comboBoxPositions.SelectedItem).PositionID;

            try
            {
                db.CheckingConnectionSql();

                bool result = db.TablePhones.InsertPhone(phone);
                results.Add(result);

                if (result)
                {
                    form.statusInsertDB.Text = "Запись успешно добавлена!";

                    form.toolStripButtonCancel.Text = "Выйти";
                    form.toolStripButtonRetry.Enabled = true;
                    form.toolStripButtonInsert.Enabled = false;

                    form.textBoxFullName.ReadOnly = true;

                    form.comboBoxPositions.Enabled = false;
                    form.comboBoxDepartments.Enabled = false;

                    form.maskedTextBoxNumberCity.Enabled = false;
                    form.maskedTextBoxNumberMobile.Enabled = false;

                    form.checkBoxNumberCity.Enabled = false;
                    form.checkBoxNumberMobile.Enabled = false;
                }
            }
            catch (Exception e)
            {
                form.statusInsertDB.Text = "Ошибка при выполнении операции с БД!";
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void ToolStripButtonRetryClick()
        {
            form.toolStripButtonRetry.Enabled = false;
            form.toolStripButtonInsert.Enabled = true;

            form.statusInsertDB.Text = String.Empty;

            form.textBoxFullName.Clear();
            form.textBoxFullName.ReadOnly = false;

            form.comboBoxDepartments.Enabled = true;
            form.comboBoxDepartments.SelectedIndex = 0;

            form.comboBoxPositions.Enabled = true;
            form.comboBoxPositions.SelectedIndex = 0;

            form.checkBoxNumberCity.Enabled = true;
            form.checkBoxNumberMobile.Enabled = true;

            form.checkBoxNumberCity.Checked = false;
            form.checkBoxNumberMobile.Checked = false;
        }

        public void CloseForm()
        {
            form.Close();
        }


        public List<bool> Results
        {
            get { return results; }
        }
    }
}
