﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using CPD.Views.Phones;
using CPD.Model;
using CPD.Model.Entities;
using CPD.Model.Tools;

namespace CPD.Controllers.Phones
{
    class ControllerFormMain
    {
        private FormMain form;
        private DbManager db;
        private Authentication admin;
        private List<EntryForEditingView> phones;

        private StringBuilder request;

        public ControllerFormMain(FormMain form)
        {
            this.form = form;
            db = DbManager.GetDbManager();

            admin = Authentication.GetInstance();
            phones = db.ViewEntriesForEditing.Rows;
            request = new StringBuilder();
            this.form.toolStripComboBoxCurrentColumn.SelectedIndex = 0;

            this.form.labelRequestSearch.Text = string.Empty;
        }

        private void LoadPhonesFromDB()
        {
            form.dataGridViewPhones.DataSource = null;

            db.ViewEntriesForEditing.GetAllPhones();

            if (!phones.Equals(db.ViewEntriesForEditing.Rows))
            {
                phones = db.ViewEntriesForEditing.Rows;
            }

            form.dataGridViewPhones.DataSource = phones;

            ConfiguringDataDisplay();
            ConfiguringEditableDataDisplay();
        }

        private void ComboBoxInitialization()
        {
            ComboBoxClearning();

            form.ComboBoxPosition.Items.AddRange(phones.ConvertAll<object>(item => item.Position).Distinct().ToArray());
            form.ComboBoxDepartment.Items.AddRange(phones.ConvertAll<object>(item => item.Department).Distinct().ToArray());

            form.ComboBoxPosition.Sorted = true;
            form.ComboBoxDepartment.Sorted = true;
        }

        private void ComboBoxClearning()
        {
            form.ComboBoxPosition.Text = String.Empty;
            form.ComboBoxPosition.Items.Clear();

            form.ComboBoxDepartment.Text = String.Empty;
            form.ComboBoxDepartment.Items.Clear();
        }

        public void OpenFormPositions()
        {
            try
            {
                db.CheckingConnectionSql();

                DbHub.HubForEntities = new Action(UpdateDataGridViewPhones);

                using (CPD.Views.Positions.FormMain formMain = new CPD.Views.Positions.FormMain())
                {
                    formMain.ShowDialog();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void OpenFormDepartments()
        {
            try
            {
                db.CheckingConnectionSql();

                DbHub.HubForEntities = new Action(UpdateDataGridViewPhones);

                using (CPD.Views.Departments.FormMain formMain = new CPD.Views.Departments.FormMain())
                {
                    formMain.ShowDialog();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void OpenFormUpdate()
        {
            if (form.dataGridViewPhones.SelectedRows.Count == 0)
            {
                MessageBox.Show("Не выбрана запись для изменения!", "Операция изменения", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            EntryForEditingView selectedPhone = (EntryForEditingView)form.dataGridViewPhones.SelectedRows[0].DataBoundItem;

            try
            {
                db.CheckingConnectionSql();

                bool blockingResult = db.TableEntriesForEditing.СheckingForBlocking(selectedPhone.PhoneID);

                if (blockingResult)
                {
                    Guid blockingAdmin = (Guid)DbHub.HubForEntities;
                    DbHub.ResetHubForEntities();

                    if (blockingAdmin != admin.CurrentAdmin.AdminID)
                    {
                        MessageBox.Show("Операция невозможна!\nЗапись находится на редактировании\nРедактирует: " + blockingAdmin + "!", "Операция изменения", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    DialogResult dialogResult = MessageBox.Show("Взять запись на редактирование и открыть в редакторе?", "Операция изменения", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (dialogResult == DialogResult.Yes)
                    {
                        DataGridViewRow selectedRow = form.dataGridViewPhones.SelectedRows[0];

                        bool insertResult = db.TableEntriesForEditing.InsertEntryForEditing(selectedPhone.PhoneID, admin.CurrentAdmin.AdminID);

                        if (insertResult)
                        {
                            selectedPhone.AdminID = admin.CurrentAdmin.AdminID;
                            selectedRow.DefaultCellStyle.BackColor = Color.LightGreen;
                        }
                    }
                    else
                    {
                        return;
                    }
                }

                DbHub.HubForEntities = selectedPhone;

                using (FormUpdate formUpdate = new FormUpdate())
                {
                    formUpdate.UpdatePhone += new Action(UpdateDataGridViewPhones);

                    formUpdate.ShowDialog();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void OpenFormInsert()
        {
            using (FormInsert formInsert = new FormInsert())
            {
                formInsert.ShowDialog();

                foreach (bool result in formInsert.ResultsInsert())
                {
                    if (result)
                    {
                        UpdateDataGridViewPhones();
                        break;
                    }
                }
            }
        }

        public void DeletingTheSelectedPhone()
        {
            if (form.dataGridViewPhones.SelectedRows.Count == 0)
            {
                MessageBox.Show("Не выбрана запись для удаления!", "Операция удаления", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            EntryForEditingView selectedPhone = (EntryForEditingView)form.dataGridViewPhones.SelectedRows[0].DataBoundItem;

            try
            {
                db.CheckingConnectionSql();

                bool blockingResult = db.TableEntriesForEditing.СheckingForBlocking(selectedPhone.PhoneID);

                if (blockingResult)
                {
                    Guid blockingAdmin = (Guid)DbHub.HubForEntities;
                    DbHub.ResetHubForEntities();

                    if (blockingAdmin == admin.CurrentAdmin.AdminID)
                    {
                        DialogResult dialogResult = MessageBox.Show("Вернуть запись в архив и удалить?", "Операция удаления", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (dialogResult == DialogResult.Yes)
                        {
                            db.TablePhones.ReturnToArchiveAndDeletePhone(selectedPhone);
                            UpdateDataGridViewPhones();
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Операция невозможна!\nЗапись находится на редактировании\nРедактирует: " + blockingAdmin + "!", "Операция удаления", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    bool deleteResult = db.TablePhones.DeletePhone(selectedPhone.PhoneID);

                    if (deleteResult)
                    {
                        MessageBox.Show("Запись успешно удалена!");
                        UpdateDataGridViewPhones();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void IsEditing()
        {
            if (form.dataGridViewPhones.SelectedRows.Count == 0)
            {
                MessageBox.Show("Необходимо выбрать запись!", "Взятие на редактирование", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DataGridViewRow selectedRow = form.dataGridViewPhones.SelectedRows[0];
            EntryForEditingView selectedPhone = (EntryForEditingView)selectedRow.DataBoundItem;

            try
            {
                db.CheckingConnectionSql();

                bool checkResult = db.TableEntriesForEditing.СheckingForBlocking(selectedPhone.PhoneID);

                if (!checkResult)
                {
                    bool insertResult = db.TableEntriesForEditing.InsertEntryForEditing(selectedPhone.PhoneID, admin.CurrentAdmin.AdminID);

                    if (insertResult)
                    {
                        selectedPhone.AdminID = admin.CurrentAdmin.AdminID;
                        selectedRow.DefaultCellStyle.BackColor = Color.LightGreen;
                    }
                }
                else
                {
                    Guid adminGUID = (Guid)DbHub.HubForEntities;
                    DbHub.ResetHubForEntities();

                    if (admin.CurrentAdmin.AdminID == adminGUID)
                    {
                        MessageBox.Show("Запись уже взята на редактирование");
                        return;
                    }

                    MessageBox.Show("Запись редактируется администратором - " + adminGUID);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void DeleteEditing()
        {
            if (form.dataGridViewPhones.SelectedRows.Count == 0)
            {
                MessageBox.Show("Необходимо выбрать запись!", "Возвращение в архив", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DataGridViewRow selectedRow = form.dataGridViewPhones.SelectedRows[0];
            EntryForEditingView selectedPhone = (EntryForEditingView)selectedRow.DataBoundItem;

            if (selectedPhone.AdminID == null)
            {
                MessageBox.Show("Запись не взята на редактирование!", "Возвращение в архив", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (selectedPhone.AdminID != admin.CurrentAdmin.AdminID)
            {
                MessageBox.Show("Запись редактируется не Вами!", "Возвращение в архив", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                db.CheckingConnectionSql();

                bool deleteResult = db.TableEntriesForEditing.DeleteEntryForEditing(selectedPhone.PhoneID, selectedPhone.AdminID ?? default(Guid));

                if (deleteResult)
                {
                    selectedPhone.AdminID = null;
                    selectedRow.DefaultCellStyle.BackColor = Color.Empty;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void UpdateDataGridViewPhones()
        {
            try
            {
                db.CheckingConnectionSql();

                LoadPhonesFromDB();

                ComboBoxInitialization();
            }
            catch (Exception e)
            {
                //Возможно только когда мы применяли фильтры, но при запросе из БД произошла ошибка!
                if (!phones.Equals(db.ViewEntriesForEditing.Rows))
                {
                    phones = db.ViewEntriesForEditing.Rows;
                }

                string message = "Ошибка обновления данных из БД!\nSQL Error:\n";

                MessageBox.Show(message + e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ConfiguringDataDisplay()
        {
            foreach (DataGridViewColumn column in form.dataGridViewPhones.Columns)
            {
                column.Resizable = DataGridViewTriState.False;
            }

            foreach (DataGridViewRow row in form.dataGridViewPhones.Rows)
            {
                row.Resizable = DataGridViewTriState.False;
                row.MinimumHeight = 25;
            }

            form.dataGridViewPhones.Columns["FullName"].HeaderText = "ФИО";
            form.dataGridViewPhones.Columns["NumberCity"].HeaderText = "Внутренний номер";
            form.dataGridViewPhones.Columns["NumberMobile"].HeaderText = "Корпоративный номер";
            form.dataGridViewPhones.Columns["Position"].HeaderText = "Должность";
            form.dataGridViewPhones.Columns["Department"].HeaderText = "Подразделение";

            //form.dataGridViewPhones.Columns["PhoneID"].Visible = false;
            //form.dataGridViewPhones.Columns["AdminID"].Visible = false;

            form.dataGridViewPhones.Columns["FullName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            form.dataGridViewPhones.Columns["NumberCity"].MinimumWidth = 100;
            form.dataGridViewPhones.Columns["NumberMobile"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            form.dataGridViewPhones.Columns["Position"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            form.dataGridViewPhones.Columns["Department"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            form.dataGridViewPhones.Columns["AdminID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            form.dataGridViewPhones.Columns["PhoneID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            form.dataGridViewPhones.ClearSelection();
        }

        private void ConfiguringEditableDataDisplay()
        {
            foreach (DataGridViewRow row in form.dataGridViewPhones.Rows)
            {
                EntryForEditingView phone = row.DataBoundItem as EntryForEditingView;

                if (phone.AdminID != null)
                {
                    if (admin.CurrentAdmin.AdminID == phone.AdminID)
                    {
                        row.DefaultCellStyle.BackColor = Color.LightGreen;
                    }
                    else
                    {
                        row.DefaultCellStyle.BackColor = Color.Yellow;
                    }
                }
            }
        }

        public void SmartSearch(KeyPressEventArgs e)
        {
            if (e.KeyChar == '/' || request.ToString().StartsWith("/"))
            {
                if (form.labelStatusSearch.Text != "ВКЛ")
                {
                    form.labelStatusSearch.Text = "ВКЛ";
                }

                if (e.KeyChar == '/' && request.ToString().StartsWith("/"))
                {
                    e.Handled = true;
                    return;
                }

                if (e.KeyChar == (char)Keys.Back)
                {
                    if (request.Length > 1)
                    {
                        request.Remove(request.Length - 1, 1);
                        form.labelRequestSearch.Text = request.ToString();

                        SearchBySelectedColumn();
                    }

                    return;
                }

                if (e.KeyChar == (char)Keys.Escape)
                {
                    request.Clear();
                    form.labelRequestSearch.Text = String.Empty;
                    form.labelStatusSearch.Text = "ВЫКЛ";
                    LoadingInitialDataSource();

                    e.Handled = true;
                    return;
                }

                form.labelRequestSearch.Text = request.Append(e.KeyChar).ToString();

                if (request.Length == 1) return;

                SearchBySelectedColumn();
            }
        }

        private void SearchBySelectedColumn()
        {
            form.dataGridViewPhones.DataSource = null;

            switch (form.toolStripComboBoxCurrentColumn.SelectedItem.ToString())
            {
                case "ФИО":
                    form.dataGridViewPhones.DataSource = phones.Where(item => item.FullName.ToUpper().Contains(request.ToString().ToUpper().Replace("/", String.Empty))).ToList();
                    break;
                case "Внутренний номер":
                    if (request.ToString().Replace("/", String.Empty) == String.Empty)
                    {
                        LoadingInitialDataSource();
                        return;
                    }

                    form.dataGridViewPhones.DataSource = phones.Where(item => item.NumberCity != null && item.NumberCity.StartsWith(request.ToString().Replace("/", String.Empty))).ToList();
                    break;
                case "Корпоративный номер":
                    if (request.ToString().Replace("/", String.Empty) == String.Empty)
                    {
                        LoadingInitialDataSource();
                        return;
                    }

                    form.dataGridViewPhones.DataSource = phones.Where(item => item.NumberMobile != null && item.NumberMobile.Contains(request.ToString().Replace("/", String.Empty))).ToList();
                    break;
                case "Должность":
                    form.dataGridViewPhones.DataSource = phones.Where(item => item.Position.ToUpper().Contains(request.ToString().ToUpper().Replace("/", String.Empty))).ToList();
                    break;
                case "Подразделение":
                    form.dataGridViewPhones.DataSource = phones.Where(item => item.Department.ToUpper().Contains(request.ToString().ToUpper().Replace("/", String.Empty))).ToList();
                    break;
            }

            ConfiguringDataDisplay();
            ConfiguringEditableDataDisplay();
        }

        private void LoadingInitialDataSource()
        {
            form.dataGridViewPhones.DataSource = null;
            form.dataGridViewPhones.DataSource = phones;
            ConfiguringDataDisplay();
            ConfiguringEditableDataDisplay();
        }

        public void Filter()
        {
            string department = form.ComboBoxDepartment.Text;
            string position = form.ComboBoxPosition.Text;

            if (position.Length == 0 && department.Length != 0)
            {
                phones = db.ViewEntriesForEditing.Rows.Where(item => item.Department == form.ComboBoxDepartment.Text).ToList();
            }
            else if (position.Length != 0 && department.Length == 0)
            {
                phones = db.ViewEntriesForEditing.Rows.Where(item => item.Position == form.ComboBoxPosition.Text).ToList();
            }
            else if (position.Length != 0 && department.Length != 0)
            {
                phones = db.ViewEntriesForEditing.Rows.Where(item => item.Position == form.ComboBoxPosition.Text && item.Department == form.ComboBoxDepartment.Text).ToList();
            }
            else
            {
                if (phones.Equals(db.ViewEntriesForEditing.Rows))
                {
                    return;
                }

                phones = db.ViewEntriesForEditing.Rows;
            }

            LoadingInitialDataSource();
        }

        public void ResetTheFilter()
        {
            form.ComboBoxPosition.Text = String.Empty;
            form.ComboBoxDepartment.Text = String.Empty;

            if (phones.Equals(db.ViewEntriesForEditing.Rows))
            {
                return;
            }

            phones = db.ViewEntriesForEditing.Rows;
            LoadingInitialDataSource();
        }

        public void ItemsOnTheEdit(object sender)
        {
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem filter = (ToolStripMenuItem)sender;

                if (filter.Checked == true)
                {
                    phones = db.ViewEntriesForEditing.Rows.Where(item => item.AdminID == admin.CurrentAdmin.AdminID).ToList();
                    LoadingInitialDataSource();
                    ComboBoxInitialization();
                }
                else
                {
                    if (!phones.Equals(db.ViewEntriesForEditing.Rows))
                    {
                        phones = db.ViewEntriesForEditing.Rows;
                        LoadingInitialDataSource();
                        ComboBoxInitialization();
                    }
                }
            }
        }


    }
}
