﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Model;
using CPD.Model.Entities;
using CPD.Model.Tools;
using CPD.Views.Phones;

namespace CPD.Controllers.Phones
{
    class ControllerFormUpdate
    {
        private FormUpdate form;
        private DbManager db;
        private EntryForEditingView updatablePhone;

        public ControllerFormUpdate(FormUpdate form)
        {
            this.form = form;
            db = DbManager.GetDbManager();

            updatablePhone = DbHub.HubForEntities as EntryForEditingView;
            DbHub.ResetHubForEntities();
        }

        public void LoadForm()
        {
            form.statusUpdateDB.Text = String.Empty;

            if (updatablePhone != null)
            {
                try
                {
                    List<Department> departments = db.TableDepartments.GetAllDepartments();
                    List<Position> positions = db.TablePositions.GetAllPositions();

                    if (departments.Count == 0 || positions.Count == 0)
                    {
                        MessageBox.Show("Справочники пусты!\nОперация невозможна", "Исключительная ситуация", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        form.Close();
                    }

                    form.comboBoxDepartments.DataSource = departments;
                    form.comboBoxPositions.DataSource = positions;

                    form.comboBoxDepartments.SelectedItem = departments.Find(item => item.Name == updatablePhone.Department);
                    form.comboBoxPositions.SelectedItem = positions.Find(item => item.Name == updatablePhone.Position);

                    form.textBoxFullName.Text = updatablePhone.FullName;

                    if (updatablePhone.NumberCity != null)
                    {
                        form.maskedTextBoxNumberCity.Text = updatablePhone.NumberCity;
                        form.checkBoxNumberCity.Checked = true;
                    }

                    if (updatablePhone.NumberMobile != null)
                    {
                        form.maskedTextBoxNumberMobile.Text = updatablePhone.NumberMobile;
                        form.checkBoxNumberMobile.Checked = true;
                    }
                }
                catch (Exception e)
                {
                    string message = "Ошибка при выполнении операции с БД!\nОкно будет закрыто. SQL Error:\n";

                    MessageBox.Show(message + e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    form.Close();
                }
            }
            else
            {
                form.toolStripButtonUpdate.Enabled = false;
                form.toolStripButtonCancel.Text = "Выйти";
            }
        }

        public void CheckBoxCheckedChanged(object sender)
        {
            if (sender is CheckBox)
            {
                CheckBox checkBox = (CheckBox)sender;

                switch (checkBox.Name)
                {
                    case "checkBoxNumberCity":
                        {
                            if (checkBox.Checked)
                            {
                                form.maskedTextBoxNumberCity.Enabled = true;
                            }
                            else
                            {
                                form.maskedTextBoxNumberCity.Enabled = false;
                            }
                        }
                        break;
                    default:
                        {
                            if (checkBox.Checked)
                            {
                                form.maskedTextBoxNumberMobile.Enabled = true;
                            }
                            else
                            {
                                form.maskedTextBoxNumberMobile.Enabled = false;
                            }
                        }
                        break;
                }
            }
        }

        public bool UpdateSelectedPhone()
        {
            if (!form.checkBoxNumberCity.Checked && !form.checkBoxNumberMobile.Checked)
            {
                form.statusUpdateDB.Text = "Один из телефонных номеров должен быть заполнен!";
                return false;
            }

            if (form.textBoxFullName.TextLength == 0)
            {
                form.statusUpdateDB.Text = "Не заполнено поле - ФИО!";
                return false;
            }

            bool result = false;
            Phone phone = new Phone();

            if (form.checkBoxNumberCity.CheckState == CheckState.Checked && form.checkBoxNumberMobile.CheckState == CheckState.Unchecked)
            {
                if (form.maskedTextBoxNumberCity.Text.Contains(" ") || form.maskedTextBoxNumberCity.TextLength != 5)
                {
                    form.statusUpdateDB.Text = "Не заполнено поле - Внутренний номер!";
                    return false;
                }

                phone.NumberCity = Convert.ToInt32(form.maskedTextBoxNumberCity.Text.Remove(2, 1));
            }
            else if (form.checkBoxNumberCity.CheckState == CheckState.Unchecked && form.checkBoxNumberMobile.CheckState == CheckState.Checked)
            {
                if (form.maskedTextBoxNumberMobile.Text.Contains(" ") || form.maskedTextBoxNumberMobile.TextLength != 16)
                {
                    form.statusUpdateDB.Text = "Не заполнено поле - Корпоративный номер!";
                    return false;
                }

                phone.NumberMobile = form.maskedTextBoxNumberMobile.Text;
            }
            else
            {
                if (form.maskedTextBoxNumberCity.Text.Contains(" ") || form.maskedTextBoxNumberCity.TextLength != 5)
                {
                    form.statusUpdateDB.Text = "Не заполнено поле - Внутренний номер!";
                    return false;
                }

                if (form.maskedTextBoxNumberMobile.Text.Contains(" ") || form.maskedTextBoxNumberMobile.TextLength != 16)
                {
                    form.statusUpdateDB.Text = "Не заполнено поле - Корпоративный номер!";
                    return false;
                }

                phone.NumberCity = Convert.ToInt32(form.maskedTextBoxNumberCity.Text.Remove(2, 1));
                phone.NumberMobile = form.maskedTextBoxNumberMobile.Text;
            }

            phone.PhoneID = updatablePhone.PhoneID;
            phone.FullName = form.textBoxFullName.Text.Trim(' ');
            phone.DepartmentID = ((Department)form.comboBoxDepartments.SelectedItem).DepartmentID;
            phone.PositionID = ((Position)form.comboBoxPositions.SelectedItem).PositionID;

            try
            {
                db.CheckingConnectionSql();

                result = db.TablePhones.UpdatePhone(phone);

                if (result)
                {
                    form.statusUpdateDB.Text = "Данные успешно изменены!";

                    form.toolStripButtonCancel.Text = "Выйти";
                    form.toolStripButtonUpdate.Enabled = false;

                    form.textBoxFullName.ReadOnly = true;

                    form.comboBoxPositions.Enabled = false;
                    form.comboBoxDepartments.Enabled = false;

                    form.maskedTextBoxNumberCity.Enabled = false;
                    form.maskedTextBoxNumberMobile.Enabled = false;

                    form.checkBoxNumberCity.Enabled = false;
                    form.checkBoxNumberMobile.Enabled = false;
                }
            }
            catch (Exception e)
            {
                form.statusUpdateDB.Text = "Ошибка при выполнении операции с БД!";
                MessageBox.Show(e.Message, "ErrorSQLMessage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return result;
        }

        public void CloseForm()
        {
            form.Close();
        }

    }
}
