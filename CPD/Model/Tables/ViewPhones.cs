﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using CPD.Model.Entities;

namespace CPD.Model.Tables
{
    class ViewPhones
    {
        private SqlConnection connection;
        private Lazy<List<PhoneView>> phones;

        public ViewPhones(SqlConnection connection)
        {
            this.connection = connection;
            phones = new Lazy<List<PhoneView>>(() => new List<PhoneView>());
        }

        public void GetAllPhones()
        {
            phones.Value.Clear();

            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "phonesView_select_all";

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        phones.Value.Add(new PhoneView()
                        {
                            FullName = reader.GetString(reader.GetOrdinal("FullName")),
                            NumberCity = reader.IsDBNull(reader.GetOrdinal("NumberCity")) ? null : reader.GetInt32(reader.GetOrdinal("NumberCity")).ToString().Insert(2, "-"),
                            NumberMobile = reader.IsDBNull(reader.GetOrdinal("NumberMobile")) ? null : reader.GetString(reader.GetOrdinal("NumberMobile")),
                            Position = reader.GetString(reader.GetOrdinal("Position")),
                            Department = reader.GetString(reader.GetOrdinal("Department")),
                        });
                    }
                }
            }
        }


        public List<PhoneView> Rows
        {
            get { return phones.Value; }
        }

    }
}
