﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CPD.Model.Entities;

namespace CPD.Model.Tables
{
    class TablePositions
    {
        private SqlConnection connection;

        public TablePositions(SqlConnection connection)
        {
            this.connection = connection;
        }

        public List<Position> GetAllPositions()
        {
            List<Position> positions = new List<Position>();

            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "positions_select_all";

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        positions.Add(new Position()
                        {
                            PositionID = reader.GetInt32(reader.GetOrdinal("PositionID")),
                            Name = reader.GetString(reader.GetOrdinal("Name"))
                        });
                    }
                }
            }

            return positions;
        }

        public List<Position> SearchForPositionsByName(string name)
        {
            List<Position> positions = new List<Position>();

            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "search_tables_like";

                command.Parameters.Add("@in_strSearch", SqlDbType.VarChar).Value = name;
                command.Parameters.Add("@in_tables", SqlDbType.VarChar).Value = "Positions";

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        positions.Add(new Position()
                        {
                            PositionID = reader.GetInt32(reader.GetOrdinal("PositionID")),
                            Name = reader.GetString(reader.GetOrdinal("Name")),
                        });
                    }
                }

            }

            return positions;
        }

        public bool InsertPosition(Position position)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "positions_insert";

                command.Parameters.Add(new SqlParameter("@in_name", SqlDbType.VarChar, 80)).Value = position.Name;

                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool UpdatePosition(Position position)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "positions_update_by_id";

                command.Parameters.Add(new SqlParameter("@in_positionID", SqlDbType.Int)).Value = position.PositionID;
                command.Parameters.Add(new SqlParameter("@in_name", SqlDbType.VarChar, 80)).Value = position.Name;

                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool DeletePosition(Position position)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "positions_delete_by_id";

                command.Parameters.Add(new SqlParameter("@in_positionID", SqlDbType.Int)).Value = position.PositionID;

                return command.ExecuteNonQuery() == 1;
            }
        }

    }
}
