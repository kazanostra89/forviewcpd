﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CPD.Model.Entities;
using CPD.Model.Tools;
using CPD.Model.Tools.Exception;

namespace CPD.Model.Tables
{
    class TableAdmins
    {
        private SqlConnection connection;

        public TableAdmins(SqlConnection connection)
        {
            this.connection = connection;
        }

        public bool? Authentication(Admin admin)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "the_administrator_authentication";

                command.Parameters.Add("@in_login", SqlDbType.VarChar, 20).Value = admin.Login;
                command.Parameters.Add("@out_result", SqlDbType.Bit).Direction = ParameterDirection.Output;
                command.Parameters.Add("@out_adminGUID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                command.Parameters.Add("@out_password", SqlDbType.VarChar, 60).Direction = ParameterDirection.Output;

                command.ExecuteNonQuery();

                object outResult = command.Parameters["@out_result"].Value;

                if (outResult is DBNull)
                {
                    return null;
                }

                object outPassword = command.Parameters["@out_password"].Value;
                object outAdminGUID = command.Parameters["@out_adminGUID"].Value;

                if (outResult is bool && (bool)outResult && outPassword is string)
                {
                    bool check = BCrypt.Net.BCrypt.Verify(admin.Password, (string)outPassword);

                    if (check && outAdminGUID is Guid)
                    {
                        Guid adminGUID = (Guid)outAdminGUID;
                        command.CommandText = "admins_update_online";
                        command.Parameters.Clear();

                        command.Parameters.Add("@in_adminGUID", SqlDbType.UniqueIdentifier).Value = adminGUID;
                        command.Parameters.Add("@in_online", SqlDbType.Bit).Value = admin.Online;

                        check = command.ExecuteNonQuery() == 1;

                        if (check)
                        {
                            admin.AdminID = adminGUID;
                            admin.Password = null;

                            return true;
                        }
                        else
                        {
                            throw new ErrorProcedureDataBaseException();
                        }
                    }
                    else
                    {
                        throw new IncorrectPasswordException();
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public List<Admin> GetAllAdmins()
        {
            List<Admin> admins = new List<Admin>();

            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "admins_select_all";

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        admins.Add(new Admin()
                        {
                            AdminID = reader.GetGuid(reader.GetOrdinal("AdminID")),
                            Login = reader.GetString(reader.GetOrdinal("Login")),
                            Online = reader.GetBoolean(reader.GetOrdinal("Online"))
                        }); 
                    }
                }
            }

            return admins;
        }

        public List<Admin> SearchForAdminsByLogin(string login)
        {
            List<Admin> admins = new List<Admin>();

            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "admins_like_login";

                command.Parameters.Add("@in_login", SqlDbType.VarChar, 20).Value = login;

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        admins.Add(new Admin()
                        {
                            AdminID = reader.GetGuid(reader.GetOrdinal("AdminID")),
                            Login = reader.GetString(reader.GetOrdinal("Login")),
                            Online = reader.GetBoolean(reader.GetOrdinal("Online"))
                        });
                    }
                }
            }

            return admins;
        }

        public bool InsertAdmin(Admin admin)
        {
            string salt = BCrypt.Net.BCrypt.GenerateSalt(5);
            string hashPassword = BCrypt.Net.BCrypt.HashPassword(admin.Password, salt);

            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "admins_insert";

                command.Parameters.Add("@in_login", SqlDbType.VarChar, 20).Value = admin.Login;
                command.Parameters.Add("@in_password", SqlDbType.VarChar, 60).Value = hashPassword;
                command.Parameters.Add("@out_result", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

                bool resultSqlRequest = command.ExecuteNonQuery() == 1;
                
                if (resultSqlRequest)
                {
                    DbHub.HubForEntities = command.Parameters["@out_result"].Value;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool UpdateAdminByGUID(Admin admin)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "admins_update_by_guid";

                command.Parameters.Add("@in_adminGUID", SqlDbType.UniqueIdentifier).Value = admin.AdminID;
                command.Parameters.Add("@in_login", SqlDbType.VarChar, 20);
                command.Parameters.Add("@in_password", SqlDbType.VarChar, 60);

                command.Parameters["@in_login"].Value = admin.Login ?? (object)DBNull.Value;

                command.Parameters["@in_password"].Value = admin.Password == null ? (object)DBNull.Value : BCrypt.Net.BCrypt.HashPassword(admin.Password, BCrypt.Net.BCrypt.GenerateSalt(5));

                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool UpdateOnlineStatus(Admin admin)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "admins_update_online";

                command.Parameters.Add("@in_adminGUID", SqlDbType.UniqueIdentifier).Value = admin.AdminID;
                command.Parameters.Add("@in_online", SqlDbType.Bit).Value = admin.Online;

                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool DeleteAdminByGUID(Admin admin)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "admins_delete_by_guid";

                command.Parameters.Add("@in_adminGUID", SqlDbType.UniqueIdentifier).Value = admin.AdminID;
                
                return command.ExecuteNonQuery() == 1;
            }
        }

    }
}
