﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CPD.Model.Entities;

namespace CPD.Model.Tables
{
    class ViewEntriesForEditing
    {
        private SqlConnection connection;
        private List<EntryForEditingView> phones;

        public ViewEntriesForEditing(SqlConnection connection)
        {
            this.connection = connection;
            phones = new List<EntryForEditingView>();
        }

        public void GetAllPhones()
        {
            phones.Clear();

            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "entriesForEditingView_select_all";
                
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        phones.Add(new EntryForEditingView()
                        {
                            PhoneID = reader.GetInt32(reader.GetOrdinal("PhoneID")),
                            FullName = reader.GetString(reader.GetOrdinal("FullName")),
                            NumberCity = reader.IsDBNull(reader.GetOrdinal("NumberCity")) ? null : reader.GetInt32(reader.GetOrdinal("NumberCity")).ToString().Insert(2, "-"),
                            NumberMobile = reader.IsDBNull(reader.GetOrdinal("NumberMobile")) ? null : reader.GetString(reader.GetOrdinal("NumberMobile")),
                            Department = reader.GetString(reader.GetOrdinal("Department")),
                            Position = reader.GetString(reader.GetOrdinal("Position")),
                            AdminID = reader.IsDBNull(reader.GetOrdinal("AdminID")) ? (Guid?)null : reader.GetGuid(reader.GetOrdinal("AdminID"))
                        });
                    }
                }
            }
        }


        public List<EntryForEditingView> Rows
        {
            get { return phones; }
        }
    }
}
