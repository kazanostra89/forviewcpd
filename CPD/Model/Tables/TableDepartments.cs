﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CPD.Model.Entities;

namespace CPD.Model.Tables
{
    class TableDepartments
    {
        private SqlConnection connection;

        public TableDepartments(SqlConnection connection)
        {
            this.connection = connection;
        }

        public List<Department> GetAllDepartments()
        {
            List<Department> departments = new List<Department>();

            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "departments_select_all";

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        departments.Add(new Department()
                        {
                            DepartmentID = reader.GetInt32(reader.GetOrdinal("DepartmentID")),
                            Name = reader.GetString(reader.GetOrdinal("Name"))
                        });
                    }
                }
            }

            return departments;
        }

        public List<Department> SearchForDepartmentsByName(string name)
        {
            List<Department> departments = new List<Department>();

            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "search_tables_like";

                command.Parameters.Add("@in_strSearch", SqlDbType.VarChar).Value = name;
                command.Parameters.Add("@in_tables", SqlDbType.VarChar).Value = "Departments";

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        departments.Add(new Department()
                        {
                            DepartmentID = reader.GetInt32(reader.GetOrdinal("DepartmentID")),
                            Name = reader.GetString(reader.GetOrdinal("Name")),
                        });
                    }
                }

            }

            return departments;
        }

        public bool InsertDepartment(Department department)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "departments_insert";

                command.Parameters.Add(new SqlParameter("@in_name", SqlDbType.VarChar, 80)).Value = department.Name;

                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool UpdateDepartment(Department department)
        {
            using(SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "departments_update_by_id";

                command.Parameters.Add(new SqlParameter("@in_departmentID", SqlDbType.Int)).Value = department.DepartmentID;
                command.Parameters.Add(new SqlParameter("@in_name", SqlDbType.VarChar, 80)).Value = department.Name;

                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool DeleteDepartment(Department department)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "department_delete_by_id";

                command.Parameters.Add(new SqlParameter("@in_departmentID", SqlDbType.Int)).Value = department.DepartmentID;

                return command.ExecuteNonQuery() == 1;
            }
        }

    }
}
