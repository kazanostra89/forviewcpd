﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CPD.Model.Entities;

namespace CPD.Model.Tables
{
    class TablePhones
    {
        private SqlConnection connection;

        public TablePhones(SqlConnection connection)
        {
            this.connection = connection;
        }

        public bool InsertPhone(Phone phone)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "phones_insert";

                command.Parameters.Add(new SqlParameter("@in_fullName", SqlDbType.VarChar)).Value = phone.FullName;
                command.Parameters.Add(new SqlParameter("@in_numberCity", SqlDbType.Int));
                command.Parameters.Add(new SqlParameter("@in_departmentID", SqlDbType.Int)).Value = phone.DepartmentID;
                command.Parameters.Add(new SqlParameter("@in_numberMobile", SqlDbType.VarChar));
                command.Parameters.Add(new SqlParameter("@in_positionID", SqlDbType.Int)).Value = phone.PositionID;

                command.Parameters["@in_numberCity"].Value = phone.NumberCity == 0 ? (object)DBNull.Value : phone.NumberCity;

                command.Parameters["@in_numberMobile"].Value = phone.NumberMobile ?? (object)DBNull.Value;

                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool UpdatePhone(Phone phone)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "phones_update_by_id";

                command.Parameters.Add(new SqlParameter("@in_phoneID", SqlDbType.Int)).Value = phone.PhoneID;
                command.Parameters.Add(new SqlParameter("@in_fullName", SqlDbType.VarChar)).Value = phone.FullName;
                command.Parameters.Add(new SqlParameter("@in_numberCity", SqlDbType.Int));
                command.Parameters.Add(new SqlParameter("@in_departmentID", SqlDbType.Int)).Value = phone.DepartmentID;
                command.Parameters.Add(new SqlParameter("@in_numberMobile", SqlDbType.VarChar));
                command.Parameters.Add(new SqlParameter("@in_positionID", SqlDbType.Int)).Value = phone.PositionID;

                command.Parameters["@in_numberCity"].Value = phone.NumberCity == 0 ? (object)DBNull.Value : phone.NumberCity;

                command.Parameters["@in_numberMobile"].Value = phone.NumberMobile ?? (object)DBNull.Value;

                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool DeletePhone(int phoneID)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "phones_delete_by_id";

                command.Parameters.Add(new SqlParameter("@in_phoneID", SqlDbType.Int)).Value = phoneID;

                return command.ExecuteNonQuery() == 1;
            }
        }

        public void ReturnToArchiveAndDeletePhone(EntryForEditingView entry)
        {
            using (SqlTransaction transaction = connection.BeginTransaction())
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.Transaction = transaction;
                    command.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        command.Parameters.Add(new SqlParameter("@in_phoneID", SqlDbType.Int)).Value = entry.PhoneID;
                        command.Parameters.Add(new SqlParameter("@in_adminID", SqlDbType.UniqueIdentifier)).Value = entry.AdminID;

                        command.CommandText = "entriesForEditing_delete";
                        command.ExecuteNonQuery();

                        command.Parameters.RemoveAt("@in_adminID");

                        command.CommandText = "phones_delete_by_id";
                        command.ExecuteNonQuery();

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

    }
}
