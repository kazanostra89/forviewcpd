﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CPD.Model.Tools;


namespace CPD.Model.Tables
{
    class TableEntriesForEditing
    {
        private SqlConnection connection;

        public TableEntriesForEditing(SqlConnection connection)
        {
            this.connection = connection;
        }

        public bool InsertEntryForEditing(int phoneID, Guid adminID)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "entriesForEditing_insert";

                command.Parameters.Add(new SqlParameter("@in_phoneID", SqlDbType.Int)).Value = phoneID;
                command.Parameters.Add(new SqlParameter("@in_adminID", SqlDbType.UniqueIdentifier)).Value = adminID;

                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool DeleteEntryForEditing(int phoneID, Guid adminID)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "entriesForEditing_delete";

                command.Parameters.Add(new SqlParameter("@in_phoneID", SqlDbType.Int)).Value = phoneID;
                command.Parameters.Add(new SqlParameter("@in_adminID", SqlDbType.UniqueIdentifier)).Value = adminID;

                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool СheckingForBlocking(int phoneID)
        {
            bool result = false;

            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "entriesForEditing_IsEditing";

                command.Parameters.Add(new SqlParameter("@in_phoneID", SqlDbType.Int)).Value = phoneID;
                command.Parameters.Add(new SqlParameter("@out_result", SqlDbType.Bit)).Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@out_adminGUID", SqlDbType.UniqueIdentifier)).Direction = ParameterDirection.Output;

                command.ExecuteNonQuery();

                object outResult = command.Parameters["@out_result"].Value;
                object outAdminGUID = command.Parameters["@out_adminGUID"].Value;

                if (outResult is bool && (bool)outResult && outAdminGUID is Guid)
                {
                    result = true;
                    DbHub.HubForEntities = outAdminGUID;
                }
            }

            return result;
        }

    }
}
