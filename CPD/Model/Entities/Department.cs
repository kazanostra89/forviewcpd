﻿using System;

namespace CPD.Model.Entities
{
    class Department
    {
        public int DepartmentID { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
