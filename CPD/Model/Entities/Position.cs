﻿using System;

namespace CPD.Model.Entities
{
    class Position
    {
        public int PositionID { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
