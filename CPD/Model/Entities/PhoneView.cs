﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPD.Model.Entities
{
    class PhoneView
    {
        public string FullName { get; set; }
        public string NumberCity { get; set; }
        public string NumberMobile { get; set; }
        public string Position { get; set; }
        public string Department { get; set; }
    }
}
