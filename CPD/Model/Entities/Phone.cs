﻿using System;

namespace CPD.Model.Entities
{
    class Phone
    {
        public int PhoneID { get; set; }
        public string FullName { get; set; }
        public int NumberCity { get; set; }
        public string NumberMobile { get; set; }
        public int DepartmentID { get; set; }
        public int PositionID { get; set; }
    }
}
