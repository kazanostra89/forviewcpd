﻿using System;

namespace CPD.Model.Entities
{
    class EntryForEditingView
    {
        public int PhoneID { get; set; }
        public string FullName { get; set; }
        public string NumberCity { get; set; }
        public string NumberMobile { get; set; }
        public string Position { get; set; }
        public string Department { get; set; }
        public Guid? AdminID { get; set; }
    }
}
