﻿using System;

namespace CPD.Model.Entities
{
    class Admin
    {
        public Guid AdminID { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool Online { get; set; }
    }
}
