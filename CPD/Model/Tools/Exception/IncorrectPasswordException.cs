﻿using System;

namespace CPD.Model.Tools.Exception
{
    [Serializable]
    class IncorrectPasswordException : System.Exception
    {
        public override string Message
        {
            get { return "Введен неверный пароль!"; }
        }
    }
}
