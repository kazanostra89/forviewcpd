﻿using System;

namespace CPD.Model.Tools.Exception
{
    [Serializable]
    class ErrorProcedureDataBaseException : System.Exception
    {
        public override string Message
        {
            get { return "Ошибка авторизации пользователя со стороны БД!"; }
        }
    }
}
