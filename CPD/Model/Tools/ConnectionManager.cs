﻿using System;
using System.Data.SqlClient;
using System.Configuration;

namespace CPD.Model.Tools
{
    static class ConnectionManager
    {
        public static SqlConnection GetConnection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["connectionCPD"].ConnectionString;

            return new SqlConnection(connectionString);
        }

    }
}
