﻿using System;
using CPD.Model.Entities;

namespace CPD.Model.Tools
{
    static class DbHub
    {
        private static object hubForEntities;

        static DbHub()
        {
            hubForEntities = null;
        }

        public static object HubForEntities
        {
            get
            {
                return hubForEntities;
            }

            set
            {
                if (value is Admin || value is Position || value is Department || value is Guid || value is EntryForEditingView || value is Action)
                {
                    hubForEntities = value;
                }
            }
        }

        public static void ResetHubForEntities()
        {
            hubForEntities = null;
        }

    }
}
