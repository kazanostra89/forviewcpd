﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPD.Model.Tools
{
    struct FieldRestrictions
    {
        public const int ADMINS_LOGIN           = 20;
        public const int ADMINS_PASSWORD        = 25;
        public const int DEPARTMENTS_NAME       = 80;
        public const int POSITIONS_NAME         = 80;
        public const int PHONES_FULL_NAME       = 40;
        public const int PHONES_NUMBER_CITY     = 4;
        public const int PHONES_NUMBER_MOBILE   = 20;
    }
}
