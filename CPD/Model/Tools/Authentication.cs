﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPD.Model.Entities;

namespace CPD.Model.Tools
{
    sealed class Authentication
    {
        private static Authentication instance = null;

        private Authentication()
        {
            if (DbHub.HubForEntities is Admin)
            {
                CurrentAdmin = (Admin)DbHub.HubForEntities;
                DbHub.ResetHubForEntities();
                Confirmed = true;
            }
        }

        public static Authentication GetInstance()
        {
            if (instance == null)
            {
                instance = new Authentication();
            }

            return instance;
        }

        public void Quit()
        {
            CurrentAdmin = null;
            Confirmed = false;
            instance = null;
        }


        public Admin CurrentAdmin { get; private set; } = null;

        public static bool Confirmed { get; private set; } = false;
    }
}
