﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CPD.Model.Tables;
using CPD.Model.Tools;

namespace CPD.Model
{
    class DbManager
    {
        private static DbManager manager = null;

        private SqlConnection connection;

        private DbManager()
        {
            connection = ConnectionManager.GetConnection();

            TableAdmins = new TableAdmins(connection);
            TablePhones = new TablePhones(connection);
            TablePositions = new TablePositions(connection);
            TableDepartments = new TableDepartments(connection);
            TableEntriesForEditing = new TableEntriesForEditing(connection);
            ViewPhones = new ViewPhones(connection);
            ViewEntriesForEditing = new ViewEntriesForEditing(connection);
        }

        public static DbManager GetDbManager()
        {
            if (manager == null)
            {
                manager = new DbManager();
            }

            return manager;
        }

        public void ConnectionOpen()
        {
            connection.Open();
        }
        
        public void ConnectionClose()
        {
            connection.Close();
        }

        public void CheckingConnectionSql()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }

        
        public TableAdmins TableAdmins { get; private set; }

        public TablePhones TablePhones { get; private set; }

        public TablePositions TablePositions { get; private set; }

        public TableDepartments TableDepartments { get; private set; }

        public TableEntriesForEditing TableEntriesForEditing { get; private set; }

        public ViewPhones ViewPhones { get; private set; }

        public ViewEntriesForEditing ViewEntriesForEditing { get; private set; }

    }
}
