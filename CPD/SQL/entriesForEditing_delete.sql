USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[entriesForEditing_delete]    Script Date: 18.10.2020 19:45:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[entriesForEditing_delete] 
	-- Add the parameters for the stored procedure here
	@in_phoneID int,
	@in_adminID uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	DELETE FROM [CPD].[dbo].[EntriesForEditing] WHERE [PhoneID] = @in_phoneID AND [AdminID] = @in_adminID
END
