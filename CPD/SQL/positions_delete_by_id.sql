USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[positions_delete_by_id]    Script Date: 18.10.2020 19:47:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[positions_delete_by_id] 
	-- Add the parameters for the stored procedure here
	@in_positionID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	DELETE FROM [CPD].[dbo].[Positions] WHERE [PositionID] = @in_positionID
END
