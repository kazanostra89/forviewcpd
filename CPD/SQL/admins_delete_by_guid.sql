USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[admins_delete_by_guid]    Script Date: 18.10.2020 19:41:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[admins_delete_by_guid] 
	-- Add the parameters for the stored procedure here
	@in_adminGUID uniqueidentifier

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	DELETE FROM [CPD].[dbo].[Admins] WHERE [AdminID] = @in_adminGUID;

END
