USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[admins_like_login]    Script Date: 18.10.2020 19:41:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[admins_like_login]
	-- Add the parameters for the stored procedure here
	@in_login varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @s_login varchar(20)
	SET @s_login = @in_login + '%'

    -- Insert statements for procedure here
	SELECT * FROM [CPD].[dbo].[Admins] WHERE [Login] LIKE @s_login
END
