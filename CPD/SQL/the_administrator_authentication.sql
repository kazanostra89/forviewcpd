USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[the_administrator_authentication]    Script Date: 18.10.2020 19:48:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[the_administrator_authentication] 
	-- Add the parameters for the stored procedure here
	@in_login varchar(20),
	@out_result bit OUTPUT,
	@out_adminGUID uniqueidentifier OUTPUT,
	@out_password varchar(60) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @check int, @adminGUID uniqueidentifier, @password varchar(60)

	SELECT @check = COUNT(*) FROM [CPD].[dbo].[Admins] WHERE [Login] = @in_login;

	IF @check = 0
		SET @out_result = NULL;
	ELSE
		BEGIN
			SELECT @adminGUID = [AdminID], @password = [Password] FROM [CPD].[dbo].[Admins] WHERE [Login] = @in_login;

			IF (SELECT [Online] FROM [CPD].[dbo].[Admins] WHERE [AdminID] = @adminGUID) = 1
				SET @out_result = 0;
			ELSE
				BEGIN
					SET @out_result = 1;
					SET @out_adminGUID = @adminGUID;
					SET @out_password = @password;
				END
		END
    
END
