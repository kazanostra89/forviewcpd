USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[search_tables_like]    Script Date: 18.10.2020 19:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[search_tables_like]
	-- Add the parameters for the stored procedure here
	@in_strSearch varchar(80),
	@in_tables varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF @in_tables = 'Admins'
		BEGIN
			SET @in_strSearch += '%';
			SELECT * FROM [CPD].[dbo].[Admins] WHERE [Login] LIKE @in_strSearch;
		END
	ELSE
		BEGIN
			IF @in_tables = 'Departments'
				BEGIN
					SET @in_strSearch = '%' + @in_strSearch + '%';
					SELECT * FROM [CPD].[dbo].[Departments] WHERE [Name] LIKE @in_strSearch;
				END
			ELSE
				BEGIN
					IF @in_tables = 'Positions'
						BEGIN
							SET @in_strSearch = '%' + @in_strSearch + '%';
							SELECT * FROM [CPD].[dbo].[Positions] WHERE [Name] LIKE @in_strSearch;
						END
				END
		END


END
