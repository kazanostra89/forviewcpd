USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[admins_update_by_guid]    Script Date: 18.10.2020 19:41:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[admins_update_by_guid]
	-- Add the parameters for the stored procedure here
	@in_adminGUID uniqueidentifier,
	@in_login varchar(20),
	@in_password varchar(60)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here

	IF NOT(@in_login IS NULL) AND @in_password IS NULL
		UPDATE [CPD].[dbo].[Admins] SET [Login] = @in_login WHERE [AdminID] = @in_adminGUID;
	ELSE
		BEGIN
			IF @in_login IS NULL AND NOT(@in_password IS NULL)
				UPDATE [CPD].[dbo].[Admins] SET [Password] = @in_password WHERE [AdminID] = @in_adminGUID;
			ELSE
				BEGIN
					IF @in_login IS NOT NULL AND @in_password IS NOT NULL
						UPDATE [CPD].[dbo].[Admins] SET [Login] = @in_login, [Password] = @in_password WHERE [AdminID] = @in_adminGUID;
				END
		END

END
