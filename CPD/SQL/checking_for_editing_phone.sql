USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[checking_for_editing_phone]    Script Date: 18.10.2020 19:44:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[checking_for_editing_phone]
	-- Add the parameters for the stored procedure here
	@in_phoneID int,
	@out_result bit OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @check int

    -- Insert statements for procedure here
	SELECT @check = COUNT(*) FROM [CPD].[dbo].[EntriesForEditing] WHERE [PhoneID] = @in_phoneID

	IF @check = 1
		SET @out_result = 1
	ELSE
		SET @out_result = 0

END
