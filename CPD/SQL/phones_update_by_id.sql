USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[phones_update_by_id]    Script Date: 18.10.2020 19:47:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[phones_update_by_id]
	-- Add the parameters for the stored procedure here
	@in_phoneID int,
	@in_fullName varchar(40),
	@in_numberCity int,
	@in_numberMobile varchar(20),
	@in_departmentID int,
	@in_positionID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE [CPD].[dbo].[Phones] SET [FullName] = @in_fullName, [NumberCity] = @in_numberCity, [NumberMobile] = @in_numberMobile, [DepartmentID] = @in_departmentID, [PositionID] = @in_positionID WHERE [PhoneID] = @in_phoneID
END
