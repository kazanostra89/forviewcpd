USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[admins_insert]    Script Date: 18.10.2020 19:41:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[admins_insert]
	-- Add the parameters for the stored procedure here
	@in_login varchar(20), 
	@in_password varchar(60),
	@out_result uniqueidentifier OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;
	DECLARE @guid TABLE([AdminID] [uniqueidentifier]);

    -- Insert statements for procedure here
	INSERT [CPD].[dbo].[Admins] ([AdminID], [Login], [Password], [Online]) OUTPUT INSERTED.[AdminID] into @guid VALUES (DEFAULT, @in_login, @in_password, DEFAULT);
	
	SET @out_result = (SELECT [AdminID] FROM @guid);

END
