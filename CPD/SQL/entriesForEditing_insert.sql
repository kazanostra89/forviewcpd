USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[entriesForEditing_insert]    Script Date: 18.10.2020 19:45:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[entriesForEditing_insert]
	-- Add the parameters for the stored procedure here
	@in_phoneID int,
	@in_adminID uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT [CPD].[dbo].[EntriesForEditing] ([PhoneID], [AdminID]) VALUES (@in_phoneID, @in_adminID)
END
