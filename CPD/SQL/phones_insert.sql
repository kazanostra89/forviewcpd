USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[phones_insert]    Script Date: 18.10.2020 19:47:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[phones_insert]
	-- Add the parameters for the stored procedure here
	@in_fullName varchar(40),
	@in_numberCity int,
	@in_numberMobile varchar(20),
	@in_departmentID int,
	@in_positionID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT [CPD].[dbo].[Phones] ([FullName], [NumberCity], [NumberMobile], [DepartmentID], [PositionID]) VALUES (@in_fullName, @in_numberCity, @in_numberMobile, @in_departmentID, @in_positionID)
END
