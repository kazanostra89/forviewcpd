USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[admins_update_online]    Script Date: 18.10.2020 19:41:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[admins_update_online]
	-- Add the parameters for the stored procedure here
	@in_adminGUID uniqueidentifier,
	@in_online bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE [CPD].[dbo].[Admins] SET [Online] = @in_online WHERE [AdminID] = @in_adminGUID
END
