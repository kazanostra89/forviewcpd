USE [CPD]
GO
/****** Object:  StoredProcedure [dbo].[departments_insert]    Script Date: 18.10.2020 19:44:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[departments_insert]
	-- Add the parameters for the stored procedure here
	@in_name varchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT [CPD].[dbo].[Departments] ([Name]) VALUES (@in_name)
END
