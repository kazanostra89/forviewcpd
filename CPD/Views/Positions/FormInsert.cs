﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Controllers.Positions;
using CPD.Model.Tools;

namespace CPD.Views.Positions
{
    public partial class FormInsert : Form
    {
        private ControllerFormInsert controller;

        public FormInsert()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormInsert(this);
            
        }

        private void FormInsert_Load(object sender, EventArgs e)
        {
            controller.LoadForm();
        }

        private void toolStripButtonInsert_Click(object sender, EventArgs e)
        {
            controller.InsertNewPosition();
        }

        private void toolStripButtonRetry_Click(object sender, EventArgs e)
        {
            controller.ToolStripButtonRetryClick();
        }

        private void toolStripButtonCancel_Click(object sender, EventArgs e)
        {
            controller.CloseForm();
        }

        private void textBoxName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (textBoxName.TextLength >= FieldRestrictions.POSITIONS_NAME
                || (e.KeyChar == (char)Keys.Space && textBoxName.Text.EndsWith(" "))
                || (e.KeyChar == (char)Keys.Space && textBoxName.TextLength == 0))
            {
                e.Handled = true;
            }
        }

        public IEnumerable<bool> ResultsInsert()
        {
            return controller.Results;
        }

    }
}
