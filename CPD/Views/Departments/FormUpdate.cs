﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Controllers.Departments;
using CPD.Model.Tools;

namespace CPD.Views.Departments
{
    public partial class FormUpdate : Form
    {
        private ControllerFormUpdate controller;

        public FormUpdate()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormUpdate(this);
        }

        private void FormUpdate_Load(object sender, EventArgs e)
        {
            controller.LoadForm();
        }

        private void toolStripButtonCancel_Click(object sender, EventArgs e)
        {
            controller.CloseForm();
        }

        public void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            bool checkUpdate = controller.UpdateSelectedDepartment();

            if (checkUpdate && UpdateDepartment != null)
            {
                UpdateDepartment();
            }
        }

        private void textBoxName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (textBoxName.TextLength >= FieldRestrictions.DEPARTMENTS_NAME
                || (e.KeyChar == (char)Keys.Space && textBoxName.Text.EndsWith(" "))
                || (e.KeyChar == (char)Keys.Space && textBoxName.TextLength == 0))
            {
                e.Handled = true;
            }
        }


        public event Action UpdateDepartment;
    }
}
