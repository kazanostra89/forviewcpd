﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Controllers.Departments;

namespace CPD.Views.Departments
{
    public partial class FormMain : Form
    {
        private ControllerFormMain controller;

        public FormMain()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormMain(this);
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            controller.CheckTheInitializationOfTheDelegate();

            controller.UpdateDataGridViewDepartments();
        }

        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            controller.DeletingTheSelectedDepartment();
        }

        private void toolStripButtonGetAllDepartments_Click(object sender, EventArgs e)
        {
            controller.UpdateDataGridViewDepartments();
        }

        private void toolStripButtonClearFilter_Click(object sender, EventArgs e)
        {
            controller.ClearTextBoxFilter();
        }

        private void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            controller.OpenFormUpdate();
        }

        private void toolStripButtonInsert_Click(object sender, EventArgs e)
        {
            controller.OpenFormInsert();
        }

        private void toolStripTextBoxFilter_TextChanged(object sender, EventArgs e)
        {
            controller.FilterForDepartments();
        }


    }
}
