﻿namespace CPD.Views.Departments
{
    partial class FormUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
            System.Windows.Forms.Label label1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUpdate));
            this.statusStripFormUpdate = new System.Windows.Forms.StatusStrip();
            this.statusUpdateDB = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripFormUpdate = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCancel = new System.Windows.Forms.ToolStripButton();
            this.textBoxName = new System.Windows.Forms.TextBox();
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            label1 = new System.Windows.Forms.Label();
            this.statusStripFormUpdate.SuspendLayout();
            this.toolStripFormUpdate.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(8, 64);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(126, 20);
            label1.TabIndex = 15;
            label1.Text = "Наименование:";
            // 
            // statusStripFormUpdate
            // 
            this.statusStripFormUpdate.AutoSize = false;
            this.statusStripFormUpdate.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStripFormUpdate.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusUpdateDB});
            this.statusStripFormUpdate.Location = new System.Drawing.Point(0, 28);
            this.statusStripFormUpdate.Name = "statusStripFormUpdate";
            this.statusStripFormUpdate.Size = new System.Drawing.Size(584, 22);
            this.statusStripFormUpdate.SizingGrip = false;
            this.statusStripFormUpdate.TabIndex = 13;
            this.statusStripFormUpdate.Text = "statusStrip1";
            // 
            // statusUpdateDB
            // 
            this.statusUpdateDB.ForeColor = System.Drawing.Color.Blue;
            this.statusUpdateDB.Name = "statusUpdateDB";
            this.statusUpdateDB.Size = new System.Drawing.Size(100, 17);
            this.statusUpdateDB.Text = "Строк состояния";
            // 
            // toolStripFormUpdate
            // 
            this.toolStripFormUpdate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripFormUpdate.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripFormUpdate.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonUpdate,
            toolStripSeparator1,
            this.toolStripButtonCancel});
            this.toolStripFormUpdate.Location = new System.Drawing.Point(0, 0);
            this.toolStripFormUpdate.Name = "toolStripFormUpdate";
            this.toolStripFormUpdate.Size = new System.Drawing.Size(584, 28);
            this.toolStripFormUpdate.TabIndex = 12;
            this.toolStripFormUpdate.Text = "toolStrip1";
            // 
            // toolStripButtonUpdate
            // 
            this.toolStripButtonUpdate.AutoSize = false;
            this.toolStripButtonUpdate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonUpdate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUpdate.Image")));
            this.toolStripButtonUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUpdate.Name = "toolStripButtonUpdate";
            this.toolStripButtonUpdate.Size = new System.Drawing.Size(280, 25);
            this.toolStripButtonUpdate.Text = "Изменить";
            this.toolStripButtonUpdate.Click += new System.EventHandler(this.toolStripButtonUpdate_Click);
            // 
            // toolStripButtonCancel
            // 
            this.toolStripButtonCancel.AutoSize = false;
            this.toolStripButtonCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonCancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCancel.Image")));
            this.toolStripButtonCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCancel.Name = "toolStripButtonCancel";
            this.toolStripButtonCancel.Size = new System.Drawing.Size(280, 25);
            this.toolStripButtonCancel.Text = "Отмена";
            this.toolStripButtonCancel.Click += new System.EventHandler(this.toolStripButtonCancel_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(8, 96);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(570, 26);
            this.textBoxName.TabIndex = 0;
            this.textBoxName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxName_KeyPress);
            // 
            // FormUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 142);
            this.Controls.Add(label1);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.statusStripFormUpdate);
            this.Controls.Add(this.toolStripFormUpdate);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактирование";
            this.Load += new System.EventHandler(this.FormUpdate_Load);
            this.statusStripFormUpdate.ResumeLayout(false);
            this.statusStripFormUpdate.PerformLayout();
            this.toolStripFormUpdate.ResumeLayout(false);
            this.toolStripFormUpdate.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStripFormUpdate;
        public System.Windows.Forms.ToolStripStatusLabel statusUpdateDB;
        private System.Windows.Forms.ToolStrip toolStripFormUpdate;
        public System.Windows.Forms.ToolStripButton toolStripButtonUpdate;
        public System.Windows.Forms.ToolStripButton toolStripButtonCancel;
        public System.Windows.Forms.TextBox textBoxName;
    }
}