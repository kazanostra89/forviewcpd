﻿namespace CPD.Views.Startup
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.buttonOpenConsoleAdmin = new System.Windows.Forms.Button();
            this.buttonQuickViewPhones = new System.Windows.Forms.Button();
            this.buttonEditingPhones = new System.Windows.Forms.Button();
            this.toolStripFormMain = new System.Windows.Forms.ToolStrip();
            this.buttonAuthentication = new System.Windows.Forms.ToolStripButton();
            this.buttonAuthenticationQuit = new System.Windows.Forms.ToolStripButton();
            this.statusStripFormMain = new System.Windows.Forms.StatusStrip();
            this.statusAuthorization = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripFormMain.SuspendLayout();
            this.statusStripFormMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOpenConsoleAdmin
            // 
            this.buttonOpenConsoleAdmin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonOpenConsoleAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOpenConsoleAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonOpenConsoleAdmin.Location = new System.Drawing.Point(8, 176);
            this.buttonOpenConsoleAdmin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonOpenConsoleAdmin.Name = "buttonOpenConsoleAdmin";
            this.buttonOpenConsoleAdmin.Size = new System.Drawing.Size(320, 55);
            this.buttonOpenConsoleAdmin.TabIndex = 0;
            this.buttonOpenConsoleAdmin.Text = "Консоль администратора";
            this.buttonOpenConsoleAdmin.UseVisualStyleBackColor = true;
            this.buttonOpenConsoleAdmin.Click += new System.EventHandler(this.buttonOpenConsoleAdmin_Click);
            // 
            // buttonQuickViewPhones
            // 
            this.buttonQuickViewPhones.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonQuickViewPhones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonQuickViewPhones.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonQuickViewPhones.Location = new System.Drawing.Point(8, 48);
            this.buttonQuickViewPhones.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonQuickViewPhones.Name = "buttonQuickViewPhones";
            this.buttonQuickViewPhones.Size = new System.Drawing.Size(320, 55);
            this.buttonQuickViewPhones.TabIndex = 2;
            this.buttonQuickViewPhones.Text = "Быстрый просмотр";
            this.buttonQuickViewPhones.UseVisualStyleBackColor = true;
            this.buttonQuickViewPhones.Click += new System.EventHandler(this.buttonQuickViewPhones_Click);
            // 
            // buttonEditingPhones
            // 
            this.buttonEditingPhones.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonEditingPhones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEditingPhones.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonEditingPhones.Location = new System.Drawing.Point(8, 112);
            this.buttonEditingPhones.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonEditingPhones.Name = "buttonEditingPhones";
            this.buttonEditingPhones.Size = new System.Drawing.Size(320, 50);
            this.buttonEditingPhones.TabIndex = 5;
            this.buttonEditingPhones.Text = "Редактор записей";
            this.buttonEditingPhones.UseVisualStyleBackColor = true;
            this.buttonEditingPhones.Click += new System.EventHandler(this.buttonEditingPhones_Click);
            // 
            // toolStripFormMain
            // 
            this.toolStripFormMain.AutoSize = false;
            this.toolStripFormMain.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripFormMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripFormMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonAuthentication,
            this.buttonAuthenticationQuit});
            this.toolStripFormMain.Location = new System.Drawing.Point(0, 0);
            this.toolStripFormMain.Name = "toolStripFormMain";
            this.toolStripFormMain.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStripFormMain.Size = new System.Drawing.Size(339, 38);
            this.toolStripFormMain.TabIndex = 6;
            this.toolStripFormMain.Text = "toolStrip1";
            // 
            // buttonAuthentication
            // 
            this.buttonAuthentication.AutoSize = false;
            this.buttonAuthentication.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonAuthentication.Image = ((System.Drawing.Image)(resources.GetObject("buttonAuthentication.Image")));
            this.buttonAuthentication.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonAuthentication.Name = "buttonAuthentication";
            this.buttonAuthentication.Size = new System.Drawing.Size(165, 35);
            this.buttonAuthentication.Text = "Авторизоваться";
            this.buttonAuthentication.Click += new System.EventHandler(this.buttonAuthentication_Click);
            // 
            // buttonAuthenticationQuit
            // 
            this.buttonAuthenticationQuit.AutoSize = false;
            this.buttonAuthenticationQuit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonAuthenticationQuit.Image = ((System.Drawing.Image)(resources.GetObject("buttonAuthenticationQuit.Image")));
            this.buttonAuthenticationQuit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonAuthenticationQuit.Name = "buttonAuthenticationQuit";
            this.buttonAuthenticationQuit.Size = new System.Drawing.Size(165, 35);
            this.buttonAuthenticationQuit.Text = "Выйти";
            this.buttonAuthenticationQuit.Click += new System.EventHandler(this.buttonAuthenticationQuit_Click);
            // 
            // statusStripFormMain
            // 
            this.statusStripFormMain.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.statusStripFormMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusAuthorization});
            this.statusStripFormMain.Location = new System.Drawing.Point(0, 246);
            this.statusStripFormMain.Name = "statusStripFormMain";
            this.statusStripFormMain.Size = new System.Drawing.Size(339, 26);
            this.statusStripFormMain.SizingGrip = false;
            this.statusStripFormMain.TabIndex = 7;
            this.statusStripFormMain.Text = "statusStrip1";
            // 
            // statusAuthorization
            // 
            this.statusAuthorization.Name = "statusAuthorization";
            this.statusAuthorization.Size = new System.Drawing.Size(111, 21);
            this.statusAuthorization.Text = "Authentication";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 272);
            this.Controls.Add(this.statusStripFormMain);
            this.Controls.Add(this.toolStripFormMain);
            this.Controls.Add(this.buttonEditingPhones);
            this.Controls.Add(this.buttonQuickViewPhones);
            this.Controls.Add(this.buttonOpenConsoleAdmin);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CPD";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.toolStripFormMain.ResumeLayout(false);
            this.toolStripFormMain.PerformLayout();
            this.statusStripFormMain.ResumeLayout(false);
            this.statusStripFormMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStripFormMain;
        private System.Windows.Forms.StatusStrip statusStripFormMain;
        public System.Windows.Forms.ToolStripButton buttonAuthentication;
        public System.Windows.Forms.ToolStripButton buttonAuthenticationQuit;
        public System.Windows.Forms.ToolStripStatusLabel statusAuthorization;
        public System.Windows.Forms.Button buttonOpenConsoleAdmin;
        public System.Windows.Forms.Button buttonQuickViewPhones;
        public System.Windows.Forms.Button buttonEditingPhones;
    }
}