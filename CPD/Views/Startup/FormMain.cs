﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using CPD.Controllers.Startup;

namespace CPD.Views.Startup
{
    public partial class FormMain : Form
    {
        private ControllerForMain controller;

        public FormMain()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerForMain(this);
        }

        private void buttonQuickViewPhones_Click(object sender, EventArgs e)
        {
            controller.OpenFastDataViewingForm();
        }

        private void buttonEditingPhones_Click(object sender, EventArgs e)
        {
            controller.OpenPhonesForm();
        }

        private void buttonOpenConsoleAdmin_Click(object sender, EventArgs e)
        {
            controller.OpenFormAdmins();
        }

        private void buttonAuthenticationQuit_Click(object sender, EventArgs e)
        {
            controller.Quit();
        }

        private void buttonAuthentication_Click(object sender, EventArgs e)
        {
            controller.OpenFormAuthorization();
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            controller.Quit();
        }
    }
}
