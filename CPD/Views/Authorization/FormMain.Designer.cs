﻿namespace CPD.Views.Authorization
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.statusStripFormMain = new System.Windows.Forms.StatusStrip();
            this.statusAuthorization = new System.Windows.Forms.ToolStripStatusLabel();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.toolStripFormMain = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonAuthorization = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCancel = new System.Windows.Forms.ToolStripButton();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.statusStripFormMain.SuspendLayout();
            this.toolStripFormMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(16, 120);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(71, 20);
            label2.TabIndex = 8;
            label2.Text = "Пароль:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(16, 72);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(59, 20);
            label1.TabIndex = 7;
            label1.Text = "Логин:";
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // statusStripFormMain
            // 
            this.statusStripFormMain.AutoSize = false;
            this.statusStripFormMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStripFormMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusAuthorization});
            this.statusStripFormMain.Location = new System.Drawing.Point(0, 28);
            this.statusStripFormMain.Name = "statusStripFormMain";
            this.statusStripFormMain.Size = new System.Drawing.Size(454, 22);
            this.statusStripFormMain.SizingGrip = false;
            this.statusStripFormMain.TabIndex = 11;
            this.statusStripFormMain.Text = "statusStrip1";
            // 
            // statusAuthorization
            // 
            this.statusAuthorization.ForeColor = System.Drawing.Color.Blue;
            this.statusAuthorization.Name = "statusAuthorization";
            this.statusAuthorization.Size = new System.Drawing.Size(100, 17);
            this.statusAuthorization.Text = "Строк состояния";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPassword.Location = new System.Drawing.Point(96, 120);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(340, 26);
            this.textBoxPassword.TabIndex = 1;
            this.textBoxPassword.Tag = "Password";
            this.textBoxPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLogin.Location = new System.Drawing.Point(96, 72);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(340, 26);
            this.textBoxLogin.TabIndex = 0;
            this.textBoxLogin.Tag = "Login";
            this.textBoxLogin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // toolStripFormMain
            // 
            this.toolStripFormMain.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripFormMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripFormMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAuthorization,
            toolStripSeparator1,
            this.toolStripButtonCancel});
            this.toolStripFormMain.Location = new System.Drawing.Point(0, 0);
            this.toolStripFormMain.Name = "toolStripFormMain";
            this.toolStripFormMain.Size = new System.Drawing.Size(454, 28);
            this.toolStripFormMain.TabIndex = 6;
            this.toolStripFormMain.Text = "toolStrip1";
            // 
            // toolStripButtonAuthorization
            // 
            this.toolStripButtonAuthorization.AutoSize = false;
            this.toolStripButtonAuthorization.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAuthorization.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAuthorization.Image")));
            this.toolStripButtonAuthorization.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAuthorization.Name = "toolStripButtonAuthorization";
            this.toolStripButtonAuthorization.Size = new System.Drawing.Size(220, 25);
            this.toolStripButtonAuthorization.Text = "Войти";
            this.toolStripButtonAuthorization.Click += new System.EventHandler(this.toolStripButtonAuthorization_Click);
            // 
            // toolStripButtonCancel
            // 
            this.toolStripButtonCancel.AutoSize = false;
            this.toolStripButtonCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonCancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCancel.Image")));
            this.toolStripButtonCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCancel.Name = "toolStripButtonCancel";
            this.toolStripButtonCancel.Size = new System.Drawing.Size(220, 25);
            this.toolStripButtonCancel.Text = "Отмена";
            this.toolStripButtonCancel.Click += new System.EventHandler(this.toolStripButtonCancel_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 167);
            this.Controls.Add(this.statusStripFormMain);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxLogin);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.toolStripFormMain);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авторизация администратора";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.statusStripFormMain.ResumeLayout(false);
            this.statusStripFormMain.PerformLayout();
            this.toolStripFormMain.ResumeLayout(false);
            this.toolStripFormMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStripFormMain;
        public System.Windows.Forms.ToolStripStatusLabel statusAuthorization;
        public System.Windows.Forms.TextBox textBoxPassword;
        public System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.ToolStrip toolStripFormMain;
        public System.Windows.Forms.ToolStripButton toolStripButtonAuthorization;
        public System.Windows.Forms.ToolStripButton toolStripButtonCancel;
    }
}