﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Controllers.Authorization;
using CPD.Model.Tools;

namespace CPD.Views.Authorization
{
    public partial class FormMain : Form
    {
        private ControllerFormMain controller;

        public FormMain()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormMain(this);
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            controller.LoadForm();
        }

        private void toolStripButtonCancel_Click(object sender, EventArgs e)
        {
            controller.CloseForm();
        }

        private void toolStripButtonAuthorization_Click(object sender, EventArgs e)
        {
            bool result = controller.AdminAuthentication();

            if (result && ResultAuthentication != null)
            {
                ResultAuthentication();
            }
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Back)
            {
                return;
            }

            if (sender is TextBox)
            {
                TextBox textBox = (TextBox)sender;
                int checkField;

                if ((string)textBox.Tag == "Login")
                {
                    checkField = FieldRestrictions.ADMINS_LOGIN;
                }
                else
                {
                    checkField = FieldRestrictions.ADMINS_PASSWORD;
                }

                if (textBox.TextLength >= checkField || e.KeyChar == (char)Keys.Space)
                {
                    e.Handled = true;
                }
            }
        }


        public event Action ResultAuthentication;
    }
}
