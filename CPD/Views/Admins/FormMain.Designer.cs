﻿namespace CPD.Views.Admins
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStripLabel toolStripLabel1;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
            this.dataGridViewAdmins = new System.Windows.Forms.DataGridView();
            this.toolStripListAdmins = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonGetAllAdmins = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonInsert = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBoxFilter = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButtonClearFilter = new System.Windows.Forms.ToolStripButton();
            this.contextMenuColumnOnline = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.switchContextMenuColumnOnline = new System.Windows.Forms.ToolStripMenuItem();
            toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdmins)).BeginInit();
            this.toolStripListAdmins.SuspendLayout();
            this.contextMenuColumnOnline.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripLabel1
            // 
            toolStripLabel1.Name = "toolStripLabel1";
            toolStripLabel1.Size = new System.Drawing.Size(57, 25);
            toolStripLabel1.Text = "Логин:";
            // 
            // dataGridViewAdmins
            // 
            this.dataGridViewAdmins.AllowUserToAddRows = false;
            this.dataGridViewAdmins.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dataGridViewAdmins.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewAdmins.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewAdmins.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAdmins.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewAdmins.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewAdmins.MultiSelect = false;
            this.dataGridViewAdmins.Name = "dataGridViewAdmins";
            this.dataGridViewAdmins.ReadOnly = true;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dataGridViewAdmins.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewAdmins.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAdmins.Size = new System.Drawing.Size(684, 484);
            this.dataGridViewAdmins.TabIndex = 0;
            // 
            // toolStripListAdmins
            // 
            this.toolStripListAdmins.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStripListAdmins.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripListAdmins.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripListAdmins.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonGetAllAdmins,
            toolStripSeparator1,
            this.toolStripButtonInsert,
            toolStripSeparator2,
            this.toolStripButtonUpdate,
            toolStripSeparator3,
            this.toolStripButtonDelete,
            toolStripSeparator4,
            toolStripLabel1,
            this.toolStripTextBoxFilter,
            this.toolStripButtonClearFilter});
            this.toolStripListAdmins.Location = new System.Drawing.Point(0, 484);
            this.toolStripListAdmins.Name = "toolStripListAdmins";
            this.toolStripListAdmins.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStripListAdmins.Size = new System.Drawing.Size(684, 28);
            this.toolStripListAdmins.TabIndex = 5;
            this.toolStripListAdmins.Text = "toolStrip1";
            // 
            // toolStripButtonGetAllAdmins
            // 
            this.toolStripButtonGetAllAdmins.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonGetAllAdmins.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonGetAllAdmins.Image")));
            this.toolStripButtonGetAllAdmins.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonGetAllAdmins.Name = "toolStripButtonGetAllAdmins";
            this.toolStripButtonGetAllAdmins.Size = new System.Drawing.Size(143, 25);
            this.toolStripButtonGetAllAdmins.Text = "Обновить данные";
            this.toolStripButtonGetAllAdmins.Click += new System.EventHandler(this.toolStripButtonGetAllAdmins_Click);
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonInsert
            // 
            this.toolStripButtonInsert.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonInsert.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonInsert.Image")));
            this.toolStripButtonInsert.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInsert.Name = "toolStripButtonInsert";
            this.toolStripButtonInsert.Size = new System.Drawing.Size(83, 25);
            this.toolStripButtonInsert.Text = "Добавить";
            this.toolStripButtonInsert.Click += new System.EventHandler(this.toolStripButtonInsert_Click);
            // 
            // toolStripSeparator2
            // 
            toolStripSeparator2.Name = "toolStripSeparator2";
            toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonUpdate
            // 
            this.toolStripButtonUpdate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonUpdate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUpdate.Image")));
            this.toolStripButtonUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUpdate.Name = "toolStripButtonUpdate";
            this.toolStripButtonUpdate.Size = new System.Drawing.Size(85, 25);
            this.toolStripButtonUpdate.Text = "Изменить";
            this.toolStripButtonUpdate.Click += new System.EventHandler(this.toolStripButtonUpdate_Click);
            // 
            // toolStripSeparator3
            // 
            toolStripSeparator3.Name = "toolStripSeparator3";
            toolStripSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonDelete
            // 
            this.toolStripButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDelete.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDelete.Image")));
            this.toolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDelete.Name = "toolStripButtonDelete";
            this.toolStripButtonDelete.Size = new System.Drawing.Size(72, 25);
            this.toolStripButtonDelete.Text = "Удалить";
            this.toolStripButtonDelete.Click += new System.EventHandler(this.toolStripButtonDelete_Click);
            // 
            // toolStripSeparator4
            // 
            toolStripSeparator4.Name = "toolStripSeparator4";
            toolStripSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripTextBoxFilter
            // 
            this.toolStripTextBoxFilter.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripTextBoxFilter.ForeColor = System.Drawing.Color.Blue;
            this.toolStripTextBoxFilter.Name = "toolStripTextBoxFilter";
            this.toolStripTextBoxFilter.Size = new System.Drawing.Size(170, 28);
            this.toolStripTextBoxFilter.TextChanged += new System.EventHandler(this.toolStripTextBoxFilter_TextChanged);
            // 
            // toolStripButtonClearFilter
            // 
            this.toolStripButtonClearFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonClearFilter.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonClearFilter.Image")));
            this.toolStripButtonClearFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClearFilter.Name = "toolStripButtonClearFilter";
            this.toolStripButtonClearFilter.Size = new System.Drawing.Size(23, 25);
            this.toolStripButtonClearFilter.Text = "Очистить фильтр поиска";
            this.toolStripButtonClearFilter.Click += new System.EventHandler(this.toolStripButtonClearFilter_Click);
            // 
            // contextMenuColumnOnline
            // 
            this.contextMenuColumnOnline.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.switchContextMenuColumnOnline});
            this.contextMenuColumnOnline.Name = "contextMenuOnline";
            this.contextMenuColumnOnline.Size = new System.Drawing.Size(166, 26);
            // 
            // switchContextMenuColumnOnline
            // 
            this.switchContextMenuColumnOnline.Name = "switchContextMenuColumnOnline";
            this.switchContextMenuColumnOnline.Size = new System.Drawing.Size(165, 22);
            this.switchContextMenuColumnOnline.Text = "Изменить статус";
            this.switchContextMenuColumnOnline.Click += new System.EventHandler(this.switchContextMenuColumnOnline_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 512);
            this.Controls.Add(this.dataGridViewAdmins);
            this.Controls.Add(this.toolStripListAdmins);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Список администраторов БД";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdmins)).EndInit();
            this.toolStripListAdmins.ResumeLayout(false);
            this.toolStripListAdmins.PerformLayout();
            this.contextMenuColumnOnline.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStripListAdmins;
        private System.Windows.Forms.ToolStripButton toolStripButtonGetAllAdmins;
        private System.Windows.Forms.ToolStripButton toolStripButtonInsert;
        private System.Windows.Forms.ToolStripButton toolStripButtonUpdate;
        private System.Windows.Forms.ToolStripButton toolStripButtonDelete;
        private System.Windows.Forms.ToolStripButton toolStripButtonClearFilter;
        public System.Windows.Forms.DataGridView dataGridViewAdmins;
        public System.Windows.Forms.ContextMenuStrip contextMenuColumnOnline;
        private System.Windows.Forms.ToolStripMenuItem switchContextMenuColumnOnline;
        public System.Windows.Forms.ToolStripTextBox toolStripTextBoxFilter;
    }
}