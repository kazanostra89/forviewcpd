﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Controllers.Admins;
using CPD.Model;

namespace CPD.Views.Admins
{
    public partial class FormMain : Form
    {
        private ControllerFormMain controller;

        public FormMain()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormMain(this);
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            controller.UpdateDataGridViewAdmins();
        }
        
        private void toolStripButtonInsert_Click(object sender, EventArgs e)
        {
            controller.OpenFormInsert();            
        }

        private void toolStripButtonGetAllAdmins_Click(object sender, EventArgs e)
        {
            controller.UpdateDataGridViewAdmins();
        }

        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            controller.DeletingTheSelectedAdministrator();
        }

        private void toolStripTextBoxFilter_TextChanged(object sender, EventArgs e)
        {
            controller.FilterForAdmins();
        }

        private void switchContextMenuColumnOnline_Click(object sender, EventArgs e)
        {
            controller.ChangingTheOnlineStatus();
        }

        private void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            controller.OpenFormUpdate();
        }

        private void toolStripButtonClearFilter_Click(object sender, EventArgs e)
        {
            controller.ClearTextBoxFilter();
        }
    }
}
