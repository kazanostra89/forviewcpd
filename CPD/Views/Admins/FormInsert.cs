﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Controllers.Admins;
using CPD.Model.Tools;

namespace CPD.Views.Admins
{
    public partial class FormInsert : Form
    {
        private ControllerFormInsert controller;

        public FormInsert()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormInsert(this);
        }

        private void FormInsert_Load(object sender, EventArgs e)
        {
            controller.LoadForm();
        }

        private void toolStripButtonInsert_Click(object sender, EventArgs e)
        {
            controller.InsertNewAdmin();
        }

        private void toolStripButtonRetry_Click(object sender, EventArgs e)
        {
            controller.ToolStripButtonRetryClick();
        }

        private void toolStripButtonCancel_Click(object sender, EventArgs e)
        {
            controller.CloseForm();
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Back)
            {
                return;
            }

            if (sender is TextBox)
            {
                TextBox textBox = (TextBox)sender;
                int checkField;

                if ((string)textBox.Tag == "Login")
                {
                    checkField = FieldRestrictions.ADMINS_LOGIN;
                }
                else
                {
                    checkField = FieldRestrictions.ADMINS_PASSWORD;
                }

                if (textBox.TextLength >= checkField || e.KeyChar == (char)Keys.Space)
                {
                    e.Handled = true;
                }
            }
        }

        public IEnumerable<bool> ResultsInsert()
        {
            return controller.Results;
        }

    }
}
