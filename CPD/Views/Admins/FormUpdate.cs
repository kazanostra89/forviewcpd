﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Model.Entities;
using CPD.Model.Tools;
using CPD.Model;
using CPD.Controllers.Admins;

namespace CPD.Views.Admins
{
    public partial class FormUpdate : Form
    {
        private ControllerFormUpdate controller;

        public FormUpdate()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormUpdate(this);
        }

        private void FormUpdate_Load(object sender, EventArgs e)
        {
            controller.LoadForm();
        }

        private void radioButtonTheFormUpdate_CheckedChanged(object sender, EventArgs e)
        {
            controller.RadioButtonCheckedChanged(sender);
        }

        private void toolStripButtonCancel_Click(object sender, EventArgs e)
        {
            controller.CloseForm();
        }

        private void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            bool checkUpdate = controller.UpdateSelectedAdmin(groupBoxFormUpdate.Controls);

            if (checkUpdate && UpdateAdmin != null)
            {
                UpdateAdmin();
            }
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox textBox = (TextBox)sender;
                int checkField;

                if ((string)textBox.Tag == "Login")
                {
                    checkField = FieldRestrictions.ADMINS_LOGIN;
                }
                else
                {
                    checkField = FieldRestrictions.ADMINS_PASSWORD;
                }

                if (e.KeyChar == (char)Keys.Back)
                {
                    return;
                }

                if (textBox.TextLength >= checkField || e.KeyChar == (char)Keys.Space)
                {
                    e.Handled = true;
                }
            }
        }

        public event Action UpdateAdmin;
    }
}
