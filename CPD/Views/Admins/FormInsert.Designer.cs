﻿namespace CPD.Views.Admins
{
    partial class FormInsert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInsert));
            this.toolStripFormInsert = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonInsert = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRetry = new System.Windows.Forms.ToolStripButton();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.statusStripFormInsert = new System.Windows.Forms.StatusStrip();
            this.statusInsertDB = new System.Windows.Forms.ToolStripStatusLabel();
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            this.toolStripFormInsert.SuspendLayout();
            this.statusStripFormInsert.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator2
            // 
            toolStripSeparator2.Name = "toolStripSeparator2";
            toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(16, 72);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(59, 20);
            label1.TabIndex = 1;
            label1.Text = "Логин:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(16, 120);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(71, 20);
            label2.TabIndex = 2;
            label2.Text = "Пароль:";
            // 
            // toolStripFormInsert
            // 
            this.toolStripFormInsert.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripFormInsert.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripFormInsert.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonInsert,
            toolStripSeparator1,
            this.toolStripButtonCancel,
            toolStripSeparator2,
            this.toolStripButtonRetry});
            this.toolStripFormInsert.Location = new System.Drawing.Point(0, 0);
            this.toolStripFormInsert.Name = "toolStripFormInsert";
            this.toolStripFormInsert.Size = new System.Drawing.Size(454, 28);
            this.toolStripFormInsert.TabIndex = 0;
            this.toolStripFormInsert.Text = "toolStrip1";
            // 
            // toolStripButtonInsert
            // 
            this.toolStripButtonInsert.AutoSize = false;
            this.toolStripButtonInsert.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonInsert.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonInsert.Image")));
            this.toolStripButtonInsert.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInsert.Name = "toolStripButtonInsert";
            this.toolStripButtonInsert.Size = new System.Drawing.Size(145, 25);
            this.toolStripButtonInsert.Text = "Добавить";
            this.toolStripButtonInsert.Click += new System.EventHandler(this.toolStripButtonInsert_Click);
            // 
            // toolStripButtonCancel
            // 
            this.toolStripButtonCancel.AutoSize = false;
            this.toolStripButtonCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonCancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCancel.Image")));
            this.toolStripButtonCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCancel.Name = "toolStripButtonCancel";
            this.toolStripButtonCancel.Size = new System.Drawing.Size(145, 25);
            this.toolStripButtonCancel.Text = "Отмена";
            this.toolStripButtonCancel.Click += new System.EventHandler(this.toolStripButtonCancel_Click);
            // 
            // toolStripButtonRetry
            // 
            this.toolStripButtonRetry.AutoSize = false;
            this.toolStripButtonRetry.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonRetry.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRetry.Image")));
            this.toolStripButtonRetry.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRetry.Name = "toolStripButtonRetry";
            this.toolStripButtonRetry.Size = new System.Drawing.Size(145, 25);
            this.toolStripButtonRetry.Text = "Повторить";
            this.toolStripButtonRetry.Click += new System.EventHandler(this.toolStripButtonRetry_Click);
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLogin.Location = new System.Drawing.Point(96, 72);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(340, 26);
            this.textBoxLogin.TabIndex = 0;
            this.textBoxLogin.Tag = "Login";
            this.textBoxLogin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPassword.Location = new System.Drawing.Point(96, 120);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(340, 26);
            this.textBoxPassword.TabIndex = 1;
            this.textBoxPassword.Tag = "Password";
            this.textBoxPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // statusStripFormInsert
            // 
            this.statusStripFormInsert.AutoSize = false;
            this.statusStripFormInsert.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStripFormInsert.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusInsertDB});
            this.statusStripFormInsert.Location = new System.Drawing.Point(0, 28);
            this.statusStripFormInsert.Name = "statusStripFormInsert";
            this.statusStripFormInsert.Size = new System.Drawing.Size(454, 22);
            this.statusStripFormInsert.SizingGrip = false;
            this.statusStripFormInsert.TabIndex = 5;
            this.statusStripFormInsert.Text = "statusStrip1";
            // 
            // statusInsertDB
            // 
            this.statusInsertDB.ForeColor = System.Drawing.Color.Blue;
            this.statusInsertDB.Name = "statusInsertDB";
            this.statusInsertDB.Size = new System.Drawing.Size(100, 17);
            this.statusInsertDB.Text = "Строк состояния";
            // 
            // FormInsert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 167);
            this.Controls.Add(this.statusStripFormInsert);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxLogin);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.toolStripFormInsert);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormInsert";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Создание администратора";
            this.Load += new System.EventHandler(this.FormInsert_Load);
            this.toolStripFormInsert.ResumeLayout(false);
            this.toolStripFormInsert.PerformLayout();
            this.statusStripFormInsert.ResumeLayout(false);
            this.statusStripFormInsert.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripFormInsert;
        private System.Windows.Forms.StatusStrip statusStripFormInsert;
        public System.Windows.Forms.ToolStripButton toolStripButtonInsert;
        public System.Windows.Forms.ToolStripButton toolStripButtonRetry;
        public System.Windows.Forms.TextBox textBoxLogin;
        public System.Windows.Forms.TextBox textBoxPassword;
        public System.Windows.Forms.ToolStripStatusLabel statusInsertDB;
        public System.Windows.Forms.ToolStripButton toolStripButtonCancel;
    }
}