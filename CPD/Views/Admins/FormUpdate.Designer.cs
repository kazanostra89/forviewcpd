﻿namespace CPD.Views.Admins
{
    partial class FormUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
            System.Windows.Forms.Label labelPassword;
            System.Windows.Forms.Label labelLogin;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUpdate));
            this.statusStripFormUpdate = new System.Windows.Forms.StatusStrip();
            this.statusUpdateDB = new System.Windows.Forms.ToolStripStatusLabel();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.toolStripFormUpdate = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCancel = new System.Windows.Forms.ToolStripButton();
            this.radioButtonLogin = new System.Windows.Forms.RadioButton();
            this.groupBoxFormUpdate = new System.Windows.Forms.GroupBox();
            this.radioButtonLoginPassword = new System.Windows.Forms.RadioButton();
            this.radioButtonPassword = new System.Windows.Forms.RadioButton();
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            labelPassword = new System.Windows.Forms.Label();
            labelLogin = new System.Windows.Forms.Label();
            this.statusStripFormUpdate.SuspendLayout();
            this.toolStripFormUpdate.SuspendLayout();
            this.groupBoxFormUpdate.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // labelPassword
            // 
            labelPassword.AutoSize = true;
            labelPassword.Location = new System.Drawing.Point(8, 88);
            labelPassword.Name = "labelPassword";
            labelPassword.Size = new System.Drawing.Size(71, 20);
            labelPassword.TabIndex = 8;
            labelPassword.Text = "Пароль:";
            // 
            // labelLogin
            // 
            labelLogin.AutoSize = true;
            labelLogin.Location = new System.Drawing.Point(8, 40);
            labelLogin.Name = "labelLogin";
            labelLogin.Size = new System.Drawing.Size(59, 20);
            labelLogin.TabIndex = 7;
            labelLogin.Text = "Логин:";
            // 
            // statusStripFormUpdate
            // 
            this.statusStripFormUpdate.AutoSize = false;
            this.statusStripFormUpdate.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStripFormUpdate.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusUpdateDB});
            this.statusStripFormUpdate.Location = new System.Drawing.Point(0, 28);
            this.statusStripFormUpdate.Name = "statusStripFormUpdate";
            this.statusStripFormUpdate.Size = new System.Drawing.Size(471, 22);
            this.statusStripFormUpdate.SizingGrip = false;
            this.statusStripFormUpdate.TabIndex = 11;
            this.statusStripFormUpdate.Text = "statusStrip1";
            // 
            // statusUpdateDB
            // 
            this.statusUpdateDB.ForeColor = System.Drawing.Color.Blue;
            this.statusUpdateDB.Name = "statusUpdateDB";
            this.statusUpdateDB.Size = new System.Drawing.Size(100, 17);
            this.statusUpdateDB.Text = "Строк состояния";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPassword.Location = new System.Drawing.Point(88, 88);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(208, 26);
            this.textBoxPassword.TabIndex = 1;
            this.textBoxPassword.Tag = "Password";
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLogin.Location = new System.Drawing.Point(88, 40);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(208, 26);
            this.textBoxLogin.TabIndex = 0;
            this.textBoxLogin.Tag = "Login";
            this.textBoxLogin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // toolStripFormUpdate
            // 
            this.toolStripFormUpdate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripFormUpdate.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripFormUpdate.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonUpdate,
            toolStripSeparator1,
            this.toolStripButtonCancel});
            this.toolStripFormUpdate.Location = new System.Drawing.Point(0, 0);
            this.toolStripFormUpdate.Name = "toolStripFormUpdate";
            this.toolStripFormUpdate.Size = new System.Drawing.Size(471, 28);
            this.toolStripFormUpdate.TabIndex = 6;
            this.toolStripFormUpdate.Text = "toolStrip1";
            // 
            // toolStripButtonUpdate
            // 
            this.toolStripButtonUpdate.AutoSize = false;
            this.toolStripButtonUpdate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonUpdate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUpdate.Image")));
            this.toolStripButtonUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUpdate.Name = "toolStripButtonUpdate";
            this.toolStripButtonUpdate.Size = new System.Drawing.Size(230, 25);
            this.toolStripButtonUpdate.Text = "Изменить";
            this.toolStripButtonUpdate.Click += new System.EventHandler(this.toolStripButtonUpdate_Click);
            // 
            // toolStripButtonCancel
            // 
            this.toolStripButtonCancel.AutoSize = false;
            this.toolStripButtonCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonCancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCancel.Image")));
            this.toolStripButtonCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCancel.Name = "toolStripButtonCancel";
            this.toolStripButtonCancel.Size = new System.Drawing.Size(230, 25);
            this.toolStripButtonCancel.Text = "Отмена";
            this.toolStripButtonCancel.Click += new System.EventHandler(this.toolStripButtonCancel_Click);
            // 
            // radioButtonLogin
            // 
            this.radioButtonLogin.AutoSize = true;
            this.radioButtonLogin.Location = new System.Drawing.Point(320, 32);
            this.radioButtonLogin.Name = "radioButtonLogin";
            this.radioButtonLogin.Size = new System.Drawing.Size(73, 24);
            this.radioButtonLogin.TabIndex = 2;
            this.radioButtonLogin.TabStop = true;
            this.radioButtonLogin.Tag = "";
            this.radioButtonLogin.Text = "Логин";
            this.radioButtonLogin.UseVisualStyleBackColor = true;
            this.radioButtonLogin.CheckedChanged += new System.EventHandler(this.radioButtonTheFormUpdate_CheckedChanged);
            // 
            // groupBoxFormUpdate
            // 
            this.groupBoxFormUpdate.Controls.Add(this.radioButtonLoginPassword);
            this.groupBoxFormUpdate.Controls.Add(this.radioButtonPassword);
            this.groupBoxFormUpdate.Controls.Add(this.textBoxPassword);
            this.groupBoxFormUpdate.Controls.Add(this.textBoxLogin);
            this.groupBoxFormUpdate.Controls.Add(this.radioButtonLogin);
            this.groupBoxFormUpdate.Controls.Add(labelPassword);
            this.groupBoxFormUpdate.Controls.Add(labelLogin);
            this.groupBoxFormUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxFormUpdate.Location = new System.Drawing.Point(0, 50);
            this.groupBoxFormUpdate.Name = "groupBoxFormUpdate";
            this.groupBoxFormUpdate.Size = new System.Drawing.Size(471, 132);
            this.groupBoxFormUpdate.TabIndex = 13;
            this.groupBoxFormUpdate.TabStop = false;
            this.groupBoxFormUpdate.Text = "Выберите вариант:";
            // 
            // radioButtonLoginPassword
            // 
            this.radioButtonLoginPassword.AutoSize = true;
            this.radioButtonLoginPassword.Location = new System.Drawing.Point(320, 96);
            this.radioButtonLoginPassword.Name = "radioButtonLoginPassword";
            this.radioButtonLoginPassword.Size = new System.Drawing.Size(145, 24);
            this.radioButtonLoginPassword.TabIndex = 4;
            this.radioButtonLoginPassword.TabStop = true;
            this.radioButtonLoginPassword.Tag = "";
            this.radioButtonLoginPassword.Text = "Логин и пароль";
            this.radioButtonLoginPassword.UseVisualStyleBackColor = true;
            this.radioButtonLoginPassword.CheckedChanged += new System.EventHandler(this.radioButtonTheFormUpdate_CheckedChanged);
            // 
            // radioButtonPassword
            // 
            this.radioButtonPassword.AutoSize = true;
            this.radioButtonPassword.Location = new System.Drawing.Point(320, 64);
            this.radioButtonPassword.Name = "radioButtonPassword";
            this.radioButtonPassword.Size = new System.Drawing.Size(85, 24);
            this.radioButtonPassword.TabIndex = 3;
            this.radioButtonPassword.TabStop = true;
            this.radioButtonPassword.Tag = "";
            this.radioButtonPassword.Text = "Пароль";
            this.radioButtonPassword.UseVisualStyleBackColor = true;
            this.radioButtonPassword.CheckedChanged += new System.EventHandler(this.radioButtonTheFormUpdate_CheckedChanged);
            // 
            // FormUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 182);
            this.Controls.Add(this.groupBoxFormUpdate);
            this.Controls.Add(this.statusStripFormUpdate);
            this.Controls.Add(this.toolStripFormUpdate);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FormUpdate_Load);
            this.statusStripFormUpdate.ResumeLayout(false);
            this.statusStripFormUpdate.PerformLayout();
            this.toolStripFormUpdate.ResumeLayout(false);
            this.toolStripFormUpdate.PerformLayout();
            this.groupBoxFormUpdate.ResumeLayout(false);
            this.groupBoxFormUpdate.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStripFormUpdate;
        public System.Windows.Forms.ToolStripStatusLabel statusUpdateDB;
        public System.Windows.Forms.TextBox textBoxPassword;
        public System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.ToolStrip toolStripFormUpdate;
        public System.Windows.Forms.ToolStripButton toolStripButtonUpdate;
        public System.Windows.Forms.ToolStripButton toolStripButtonCancel;
        private System.Windows.Forms.GroupBox groupBoxFormUpdate;
        public System.Windows.Forms.RadioButton radioButtonLogin;
        public System.Windows.Forms.RadioButton radioButtonLoginPassword;
        public System.Windows.Forms.RadioButton radioButtonPassword;
    }
}