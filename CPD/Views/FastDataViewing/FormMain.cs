﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Controllers.FastDataViewing;

namespace CPD.Views.FastDataViewing
{
    public partial class FormMain : Form
    {
        private ControllerForMain controller;

        public FormMain()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerForMain(this);
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            controller.LoadForm();
        }

        private void buttonGetAllPhones_Click(object sender, EventArgs e)
        {
            controller.UpdateDataGridViewPhones();
        }

        
        private void dataGridViewPhones_KeyPress(object sender, KeyPressEventArgs e)
        {
            controller.SmartSearch(e);
        }

        private void buttonFilterCleaning_Click(object sender, EventArgs e)
        {
            controller.ResetTheFilter();
        }

        private void toolStripButtonApplyFilter_Click(object sender, EventArgs e)
        {
            controller.Filter();
        }
    }
}
