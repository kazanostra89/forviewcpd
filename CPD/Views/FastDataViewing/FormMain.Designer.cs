﻿namespace CPD.Views.FastDataViewing
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.dataGridViewPhones = new System.Windows.Forms.DataGridView();
            this.toolStripFormMain = new System.Windows.Forms.ToolStrip();
            this.labelStatusSearch = new System.Windows.Forms.ToolStripLabel();
            this.buttonGetAllPhones = new System.Windows.Forms.ToolStripButton();
            this.ComboBoxDepartment = new System.Windows.Forms.ToolStripComboBox();
            this.ComboBoxPosition = new System.Windows.Forms.ToolStripComboBox();
            this.buttonFilterCleaning = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonApplyFilter = new System.Windows.Forms.ToolStripButton();
            this.labelCurrentColumn = new System.Windows.Forms.ToolStripLabel();
            this.labelRequestSearch = new System.Windows.Forms.ToolStripLabel();
            toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPhones)).BeginInit();
            this.toolStripFormMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator6
            // 
            toolStripSeparator6.Name = "toolStripSeparator6";
            toolStripSeparator6.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator2
            // 
            toolStripSeparator2.Name = "toolStripSeparator2";
            toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator3
            // 
            toolStripSeparator3.Name = "toolStripSeparator3";
            toolStripSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator4
            // 
            toolStripSeparator4.Name = "toolStripSeparator4";
            toolStripSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator5
            // 
            toolStripSeparator5.Name = "toolStripSeparator5";
            toolStripSeparator5.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // dataGridViewPhones
            // 
            this.dataGridViewPhones.AllowUserToAddRows = false;
            this.dataGridViewPhones.AllowUserToDeleteRows = false;
            this.dataGridViewPhones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPhones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPhones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPhones.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewPhones.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewPhones.MultiSelect = false;
            this.dataGridViewPhones.Name = "dataGridViewPhones";
            this.dataGridViewPhones.ReadOnly = true;
            this.dataGridViewPhones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewPhones.Size = new System.Drawing.Size(1035, 537);
            this.dataGridViewPhones.TabIndex = 0;
            this.dataGridViewPhones.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridViewPhones_KeyPress);
            // 
            // toolStripFormMain
            // 
            this.toolStripFormMain.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStripFormMain.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripFormMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripFormMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelStatusSearch,
            toolStripSeparator6,
            this.buttonGetAllPhones,
            toolStripSeparator2,
            this.ComboBoxDepartment,
            toolStripSeparator3,
            this.ComboBoxPosition,
            toolStripSeparator4,
            this.buttonFilterCleaning,
            this.toolStripButtonApplyFilter,
            toolStripSeparator5,
            this.labelCurrentColumn,
            toolStripSeparator1,
            this.labelRequestSearch});
            this.toolStripFormMain.Location = new System.Drawing.Point(0, 509);
            this.toolStripFormMain.Name = "toolStripFormMain";
            this.toolStripFormMain.Size = new System.Drawing.Size(1035, 28);
            this.toolStripFormMain.TabIndex = 5;
            this.toolStripFormMain.Text = "toolStrip1";
            // 
            // labelStatusSearch
            // 
            this.labelStatusSearch.Name = "labelStatusSearch";
            this.labelStatusSearch.Size = new System.Drawing.Size(52, 25);
            this.labelStatusSearch.Text = "ВЫКЛ";
            // 
            // buttonGetAllPhones
            // 
            this.buttonGetAllPhones.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonGetAllPhones.Image = ((System.Drawing.Image)(resources.GetObject("buttonGetAllPhones.Image")));
            this.buttonGetAllPhones.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonGetAllPhones.Name = "buttonGetAllPhones";
            this.buttonGetAllPhones.Size = new System.Drawing.Size(23, 25);
            this.buttonGetAllPhones.Text = "toolStripButton2";
            this.buttonGetAllPhones.Click += new System.EventHandler(this.buttonGetAllPhones_Click);
            // 
            // ComboBoxDepartment
            // 
            this.ComboBoxDepartment.AutoSize = false;
            this.ComboBoxDepartment.Name = "ComboBoxDepartment";
            this.ComboBoxDepartment.Size = new System.Drawing.Size(170, 23);
            // 
            // ComboBoxPosition
            // 
            this.ComboBoxPosition.AutoSize = false;
            this.ComboBoxPosition.Name = "ComboBoxPosition";
            this.ComboBoxPosition.Size = new System.Drawing.Size(170, 23);
            // 
            // buttonFilterCleaning
            // 
            this.buttonFilterCleaning.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonFilterCleaning.Image = ((System.Drawing.Image)(resources.GetObject("buttonFilterCleaning.Image")));
            this.buttonFilterCleaning.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonFilterCleaning.Name = "buttonFilterCleaning";
            this.buttonFilterCleaning.Size = new System.Drawing.Size(23, 25);
            this.buttonFilterCleaning.Text = "toolStripButton2";
            this.buttonFilterCleaning.Click += new System.EventHandler(this.buttonFilterCleaning_Click);
            // 
            // toolStripButtonApplyFilter
            // 
            this.toolStripButtonApplyFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonApplyFilter.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonApplyFilter.Image")));
            this.toolStripButtonApplyFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonApplyFilter.Name = "toolStripButtonApplyFilter";
            this.toolStripButtonApplyFilter.Size = new System.Drawing.Size(95, 25);
            this.toolStripButtonApplyFilter.Text = "Применить";
            this.toolStripButtonApplyFilter.Click += new System.EventHandler(this.toolStripButtonApplyFilter_Click);
            // 
            // labelCurrentColumn
            // 
            this.labelCurrentColumn.Name = "labelCurrentColumn";
            this.labelCurrentColumn.Size = new System.Drawing.Size(70, 25);
            this.labelCurrentColumn.Text = "Колонка";
            // 
            // labelRequestSearch
            // 
            this.labelRequestSearch.Name = "labelRequestSearch";
            this.labelRequestSearch.Size = new System.Drawing.Size(61, 25);
            this.labelRequestSearch.Text = "Запрос";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1035, 537);
            this.Controls.Add(this.toolStripFormMain);
            this.Controls.Add(this.dataGridViewPhones);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Быстрый просмотр";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPhones)).EndInit();
            this.toolStripFormMain.ResumeLayout(false);
            this.toolStripFormMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView dataGridViewPhones;
        private System.Windows.Forms.ToolStrip toolStripFormMain;
        public System.Windows.Forms.ToolStripLabel labelCurrentColumn;
        public System.Windows.Forms.ToolStripLabel labelRequestSearch;
        public System.Windows.Forms.ToolStripLabel labelStatusSearch;
        public System.Windows.Forms.ToolStripComboBox ComboBoxPosition;
        public System.Windows.Forms.ToolStripComboBox ComboBoxDepartment;
        private System.Windows.Forms.ToolStripButton toolStripButtonApplyFilter;
        private System.Windows.Forms.ToolStripButton buttonFilterCleaning;
        private System.Windows.Forms.ToolStripButton buttonGetAllPhones;
    }
}