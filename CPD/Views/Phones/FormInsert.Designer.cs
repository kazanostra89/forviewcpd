﻿namespace CPD.Views.Phones
{
    partial class FormInsert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInsert));
            this.toolStripFormInsert = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonInsert = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRetry = new System.Windows.Forms.ToolStripButton();
            this.statusStripFormInsert = new System.Windows.Forms.StatusStrip();
            this.statusInsertDB = new System.Windows.Forms.ToolStripStatusLabel();
            this.textBoxFullName = new System.Windows.Forms.TextBox();
            this.maskedTextBoxNumberMobile = new System.Windows.Forms.MaskedTextBox();
            this.comboBoxDepartments = new System.Windows.Forms.ComboBox();
            this.comboBoxPositions = new System.Windows.Forms.ComboBox();
            this.maskedTextBoxNumberCity = new System.Windows.Forms.MaskedTextBox();
            this.checkBoxNumberCity = new System.Windows.Forms.CheckBox();
            this.checkBoxNumberMobile = new System.Windows.Forms.CheckBox();
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            this.toolStripFormInsert.SuspendLayout();
            this.statusStripFormInsert.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator2
            // 
            toolStripSeparator2.Name = "toolStripSeparator2";
            toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(8, 64);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(149, 20);
            label1.TabIndex = 17;
            label1.Text = "ФИО (полностью):";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(8, 288);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(154, 20);
            label2.TabIndex = 18;
            label2.Text = "Внутренний номер:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(232, 288);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(184, 20);
            label3.TabIndex = 19;
            label3.Text = "Корпоративный номер:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(8, 136);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(137, 20);
            label4.TabIndex = 24;
            label4.Text = "Подразделение:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(8, 208);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(99, 20);
            label5.TabIndex = 25;
            label5.Text = "Должность:";
            // 
            // toolStripFormInsert
            // 
            this.toolStripFormInsert.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripFormInsert.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripFormInsert.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonInsert,
            toolStripSeparator1,
            this.toolStripButtonCancel,
            toolStripSeparator2,
            this.toolStripButtonRetry});
            this.toolStripFormInsert.Location = new System.Drawing.Point(0, 0);
            this.toolStripFormInsert.Name = "toolStripFormInsert";
            this.toolStripFormInsert.Size = new System.Drawing.Size(584, 28);
            this.toolStripFormInsert.TabIndex = 0;
            this.toolStripFormInsert.Text = "toolStrip1";
            // 
            // toolStripButtonInsert
            // 
            this.toolStripButtonInsert.AutoSize = false;
            this.toolStripButtonInsert.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonInsert.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonInsert.Image")));
            this.toolStripButtonInsert.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInsert.Name = "toolStripButtonInsert";
            this.toolStripButtonInsert.Size = new System.Drawing.Size(190, 25);
            this.toolStripButtonInsert.Text = "Добавить";
            this.toolStripButtonInsert.Click += new System.EventHandler(this.toolStripButtonInsert_Click);
            // 
            // toolStripButtonCancel
            // 
            this.toolStripButtonCancel.AutoSize = false;
            this.toolStripButtonCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonCancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCancel.Image")));
            this.toolStripButtonCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCancel.Name = "toolStripButtonCancel";
            this.toolStripButtonCancel.Size = new System.Drawing.Size(190, 25);
            this.toolStripButtonCancel.Text = "Отмена";
            this.toolStripButtonCancel.Click += new System.EventHandler(this.toolStripButtonCancel_Click);
            // 
            // toolStripButtonRetry
            // 
            this.toolStripButtonRetry.AutoSize = false;
            this.toolStripButtonRetry.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonRetry.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRetry.Image")));
            this.toolStripButtonRetry.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRetry.Name = "toolStripButtonRetry";
            this.toolStripButtonRetry.Size = new System.Drawing.Size(190, 25);
            this.toolStripButtonRetry.Text = "Повторить";
            this.toolStripButtonRetry.Click += new System.EventHandler(this.toolStripButtonRetry_Click);
            // 
            // statusStripFormInsert
            // 
            this.statusStripFormInsert.AutoSize = false;
            this.statusStripFormInsert.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStripFormInsert.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusInsertDB});
            this.statusStripFormInsert.Location = new System.Drawing.Point(0, 28);
            this.statusStripFormInsert.Name = "statusStripFormInsert";
            this.statusStripFormInsert.Size = new System.Drawing.Size(584, 22);
            this.statusStripFormInsert.SizingGrip = false;
            this.statusStripFormInsert.TabIndex = 5;
            this.statusStripFormInsert.Text = "statusStrip1";
            // 
            // statusInsertDB
            // 
            this.statusInsertDB.ForeColor = System.Drawing.Color.Blue;
            this.statusInsertDB.Name = "statusInsertDB";
            this.statusInsertDB.Size = new System.Drawing.Size(100, 17);
            this.statusInsertDB.Text = "Строк состояния";
            // 
            // textBoxFullName
            // 
            this.textBoxFullName.Location = new System.Drawing.Point(8, 96);
            this.textBoxFullName.Name = "textBoxFullName";
            this.textBoxFullName.Size = new System.Drawing.Size(570, 26);
            this.textBoxFullName.TabIndex = 0;
            this.textBoxFullName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxName_KeyPress);
            // 
            // maskedTextBoxNumberMobile
            // 
            this.maskedTextBoxNumberMobile.Enabled = false;
            this.maskedTextBoxNumberMobile.Location = new System.Drawing.Point(424, 288);
            this.maskedTextBoxNumberMobile.Mask = "+7(999)000-00-00";
            this.maskedTextBoxNumberMobile.Name = "maskedTextBoxNumberMobile";
            this.maskedTextBoxNumberMobile.Size = new System.Drawing.Size(136, 26);
            this.maskedTextBoxNumberMobile.TabIndex = 4;
            // 
            // comboBoxDepartments
            // 
            this.comboBoxDepartments.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepartments.FormattingEnabled = true;
            this.comboBoxDepartments.Location = new System.Drawing.Point(8, 168);
            this.comboBoxDepartments.Name = "comboBoxDepartments";
            this.comboBoxDepartments.Size = new System.Drawing.Size(570, 28);
            this.comboBoxDepartments.TabIndex = 1;
            // 
            // comboBoxPositions
            // 
            this.comboBoxPositions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPositions.FormattingEnabled = true;
            this.comboBoxPositions.Location = new System.Drawing.Point(8, 240);
            this.comboBoxPositions.Name = "comboBoxPositions";
            this.comboBoxPositions.Size = new System.Drawing.Size(570, 28);
            this.comboBoxPositions.TabIndex = 2;
            // 
            // maskedTextBoxNumberCity
            // 
            this.maskedTextBoxNumberCity.Enabled = false;
            this.maskedTextBoxNumberCity.Location = new System.Drawing.Point(160, 288);
            this.maskedTextBoxNumberCity.Mask = "00-00";
            this.maskedTextBoxNumberCity.Name = "maskedTextBoxNumberCity";
            this.maskedTextBoxNumberCity.Size = new System.Drawing.Size(48, 26);
            this.maskedTextBoxNumberCity.TabIndex = 3;
            // 
            // checkBoxNumberCity
            // 
            this.checkBoxNumberCity.AutoSize = true;
            this.checkBoxNumberCity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxNumberCity.Location = new System.Drawing.Point(216, 296);
            this.checkBoxNumberCity.Name = "checkBoxNumberCity";
            this.checkBoxNumberCity.Size = new System.Drawing.Size(12, 11);
            this.checkBoxNumberCity.TabIndex = 27;
            this.checkBoxNumberCity.TabStop = false;
            this.checkBoxNumberCity.UseVisualStyleBackColor = true;
            this.checkBoxNumberCity.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxNumberMobile
            // 
            this.checkBoxNumberMobile.AutoSize = true;
            this.checkBoxNumberMobile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxNumberMobile.Location = new System.Drawing.Point(568, 296);
            this.checkBoxNumberMobile.Name = "checkBoxNumberMobile";
            this.checkBoxNumberMobile.Size = new System.Drawing.Size(12, 11);
            this.checkBoxNumberMobile.TabIndex = 28;
            this.checkBoxNumberMobile.TabStop = false;
            this.checkBoxNumberMobile.UseVisualStyleBackColor = true;
            this.checkBoxNumberMobile.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // FormInsert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 332);
            this.Controls.Add(this.checkBoxNumberMobile);
            this.Controls.Add(this.checkBoxNumberCity);
            this.Controls.Add(this.maskedTextBoxNumberCity);
            this.Controls.Add(label5);
            this.Controls.Add(label4);
            this.Controls.Add(this.comboBoxPositions);
            this.Controls.Add(this.comboBoxDepartments);
            this.Controls.Add(this.maskedTextBoxNumberMobile);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.textBoxFullName);
            this.Controls.Add(this.statusStripFormInsert);
            this.Controls.Add(this.toolStripFormInsert);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormInsert";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Создание нового телефона";
            this.Load += new System.EventHandler(this.FormInsert_Load);
            this.toolStripFormInsert.ResumeLayout(false);
            this.toolStripFormInsert.PerformLayout();
            this.statusStripFormInsert.ResumeLayout(false);
            this.statusStripFormInsert.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripFormInsert;
        private System.Windows.Forms.StatusStrip statusStripFormInsert;
        public System.Windows.Forms.ToolStripButton toolStripButtonInsert;
        public System.Windows.Forms.ToolStripButton toolStripButtonRetry;
        public System.Windows.Forms.ToolStripStatusLabel statusInsertDB;
        public System.Windows.Forms.ToolStripButton toolStripButtonCancel;
        public System.Windows.Forms.TextBox textBoxFullName;
        public System.Windows.Forms.MaskedTextBox maskedTextBoxNumberMobile;
        public System.Windows.Forms.ComboBox comboBoxDepartments;
        public System.Windows.Forms.ComboBox comboBoxPositions;
        public System.Windows.Forms.MaskedTextBox maskedTextBoxNumberCity;
        public System.Windows.Forms.CheckBox checkBoxNumberCity;
        public System.Windows.Forms.CheckBox checkBoxNumberMobile;
    }
}