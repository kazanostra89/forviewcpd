﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Controllers.Phones;
using CPD.Model.Tools;

namespace CPD.Views.Phones
{
    public partial class FormInsert : Form
    {
        private ControllerFormInsert controller;

        public FormInsert()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormInsert(this);
        }

        private void FormInsert_Load(object sender, EventArgs e)
        {
            controller.LoadForm();
        }

        private void toolStripButtonInsert_Click(object sender, EventArgs e)
        {
            controller.InsertNewPhone();
        }

        private void toolStripButtonRetry_Click(object sender, EventArgs e)
        {
            controller.ToolStripButtonRetryClick();
        }

        private void toolStripButtonCancel_Click(object sender, EventArgs e)
        {
            controller.CloseForm();
        }

        private void textBoxName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= (char)1040 && e.KeyChar <= (char)1103 && textBoxFullName.TextLength < FieldRestrictions.PHONES_FULL_NAME) || (e.KeyChar == (char)Keys.Space && textBoxFullName.TextLength < FieldRestrictions.PHONES_FULL_NAME)
                || e.KeyChar == (char)Keys.Back)
            {

                if ((e.KeyChar == (char)Keys.Space && textBoxFullName.Text.EndsWith(" "))
                    || (e.KeyChar == (char)Keys.Space && textBoxFullName.TextLength == 0))
                {
                    e.Handled = true;
                }

                return;
            }

            e.Handled = true;
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            controller.CheckBoxCheckedChanged(sender);
        }

        public IEnumerable<bool> ResultsInsert()
        {
            return controller.Results;
        }
    }
}
