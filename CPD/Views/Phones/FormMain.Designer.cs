﻿namespace CPD.Views.Phones
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
            System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.dataGridViewPhones = new System.Windows.Forms.DataGridView();
            this.contextMenuStripCommand = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.insertNewPhoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.takeForEditingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.returnToTheArchiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editPhoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deletePhoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getAllPhonesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripFormMaim = new System.Windows.Forms.MenuStrip();
            this.directoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.positionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.departmentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanelFormMain = new System.Windows.Forms.TableLayoutPanel();
            this.toolStripFormMain = new System.Windows.Forms.ToolStrip();
            this.labelStatusSearch = new System.Windows.Forms.ToolStripLabel();
            this.buttonGetAllPhones = new System.Windows.Forms.ToolStripButton();
            this.ComboBoxDepartment = new System.Windows.Forms.ToolStripComboBox();
            this.ComboBoxPosition = new System.Windows.Forms.ToolStripComboBox();
            this.buttonFilterCleaning = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonApplyFilter = new System.Windows.Forms.ToolStripButton();
            this.toolStripComboBoxCurrentColumn = new System.Windows.Forms.ToolStripComboBox();
            this.labelRequestSearch = new System.Windows.Forms.ToolStripLabel();
            this.commandsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemsOnTheEditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPhones)).BeginInit();
            this.contextMenuStripCommand.SuspendLayout();
            this.menuStripFormMaim.SuspendLayout();
            this.tableLayoutPanelFormMain.SuspendLayout();
            this.toolStripFormMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator6
            // 
            toolStripSeparator6.Name = "toolStripSeparator6";
            toolStripSeparator6.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator2
            // 
            toolStripSeparator2.Name = "toolStripSeparator2";
            toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator3
            // 
            toolStripSeparator3.Name = "toolStripSeparator3";
            toolStripSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator4
            // 
            toolStripSeparator4.Name = "toolStripSeparator4";
            toolStripSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator5
            // 
            toolStripSeparator5.Name = "toolStripSeparator5";
            toolStripSeparator5.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // dataGridViewPhones
            // 
            this.dataGridViewPhones.AllowUserToAddRows = false;
            this.dataGridViewPhones.AllowUserToDeleteRows = false;
            this.dataGridViewPhones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPhones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPhones.ContextMenuStrip = this.contextMenuStripCommand;
            this.dataGridViewPhones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPhones.Location = new System.Drawing.Point(4, 35);
            this.dataGridViewPhones.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridViewPhones.Name = "dataGridViewPhones";
            this.dataGridViewPhones.ReadOnly = true;
            this.dataGridViewPhones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPhones.Size = new System.Drawing.Size(1429, 523);
            this.dataGridViewPhones.TabIndex = 0;
            this.dataGridViewPhones.TabStop = false;
            this.dataGridViewPhones.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridViewPhones_KeyPress);
            // 
            // contextMenuStripCommand
            // 
            this.contextMenuStripCommand.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insertNewPhoneToolStripMenuItem,
            this.takeForEditingToolStripMenuItem,
            this.returnToTheArchiveToolStripMenuItem,
            this.editPhoneToolStripMenuItem,
            this.deletePhoneToolStripMenuItem,
            this.getAllPhonesToolStripMenuItem});
            this.contextMenuStripCommand.Name = "contextMenuStripCommand";
            this.contextMenuStripCommand.Size = new System.Drawing.Size(212, 136);
            // 
            // insertNewPhoneToolStripMenuItem
            // 
            this.insertNewPhoneToolStripMenuItem.Name = "insertNewPhoneToolStripMenuItem";
            this.insertNewPhoneToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.insertNewPhoneToolStripMenuItem.Text = "Новая запись";
            this.insertNewPhoneToolStripMenuItem.Click += new System.EventHandler(this.insertNewPhoneToolStripMenuItem_Click);
            // 
            // takeForEditingToolStripMenuItem
            // 
            this.takeForEditingToolStripMenuItem.Name = "takeForEditingToolStripMenuItem";
            this.takeForEditingToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.takeForEditingToolStripMenuItem.Text = "Взять на редактирование";
            this.takeForEditingToolStripMenuItem.Click += new System.EventHandler(this.takeForEditingToolStripMenuItem_Click);
            // 
            // returnToTheArchiveToolStripMenuItem
            // 
            this.returnToTheArchiveToolStripMenuItem.Name = "returnToTheArchiveToolStripMenuItem";
            this.returnToTheArchiveToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.returnToTheArchiveToolStripMenuItem.Text = "Вернуть в архив";
            this.returnToTheArchiveToolStripMenuItem.Click += new System.EventHandler(this.returnToTheArchiveToolStripMenuItem_Click);
            // 
            // editPhoneToolStripMenuItem
            // 
            this.editPhoneToolStripMenuItem.Name = "editPhoneToolStripMenuItem";
            this.editPhoneToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.editPhoneToolStripMenuItem.Text = "Редактировать";
            this.editPhoneToolStripMenuItem.Click += new System.EventHandler(this.editPhoneToolStripMenuItem_Click);
            // 
            // deletePhoneToolStripMenuItem
            // 
            this.deletePhoneToolStripMenuItem.Name = "deletePhoneToolStripMenuItem";
            this.deletePhoneToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.deletePhoneToolStripMenuItem.Text = "Удалить";
            this.deletePhoneToolStripMenuItem.Click += new System.EventHandler(this.deletePhoneToolStripMenuItem_Click);
            // 
            // getAllPhonesToolStripMenuItem
            // 
            this.getAllPhonesToolStripMenuItem.Name = "getAllPhonesToolStripMenuItem";
            this.getAllPhonesToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.getAllPhonesToolStripMenuItem.Text = "Обновить";
            this.getAllPhonesToolStripMenuItem.Click += new System.EventHandler(this.getAllPhonesToolStripMenuItem_Click);
            // 
            // menuStripFormMaim
            // 
            this.menuStripFormMaim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStripFormMaim.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStripFormMaim.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.directoryToolStripMenuItem,
            this.commandsToolStripMenuItem});
            this.menuStripFormMaim.Location = new System.Drawing.Point(0, 0);
            this.menuStripFormMaim.Name = "menuStripFormMaim";
            this.menuStripFormMaim.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStripFormMaim.Size = new System.Drawing.Size(1437, 30);
            this.menuStripFormMaim.TabIndex = 2;
            this.menuStripFormMaim.Text = "menuStrip1";
            // 
            // directoryToolStripMenuItem
            // 
            this.directoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.positionsToolStripMenuItem,
            this.departmentsToolStripMenuItem});
            this.directoryToolStripMenuItem.Name = "directoryToolStripMenuItem";
            this.directoryToolStripMenuItem.Size = new System.Drawing.Size(119, 26);
            this.directoryToolStripMenuItem.Text = "Справочники";
            // 
            // positionsToolStripMenuItem
            // 
            this.positionsToolStripMenuItem.Name = "positionsToolStripMenuItem";
            this.positionsToolStripMenuItem.Size = new System.Drawing.Size(192, 26);
            this.positionsToolStripMenuItem.Text = "Должности";
            this.positionsToolStripMenuItem.Click += new System.EventHandler(this.positionsToolStripMenuItem_Click);
            // 
            // departmentsToolStripMenuItem
            // 
            this.departmentsToolStripMenuItem.Name = "departmentsToolStripMenuItem";
            this.departmentsToolStripMenuItem.Size = new System.Drawing.Size(192, 26);
            this.departmentsToolStripMenuItem.Text = "Подразделения";
            this.departmentsToolStripMenuItem.Click += new System.EventHandler(this.departmentsToolStripMenuItem_Click);
            // 
            // tableLayoutPanelFormMain
            // 
            this.tableLayoutPanelFormMain.ColumnCount = 1;
            this.tableLayoutPanelFormMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelFormMain.Controls.Add(this.toolStripFormMain, 0, 2);
            this.tableLayoutPanelFormMain.Controls.Add(this.dataGridViewPhones, 0, 1);
            this.tableLayoutPanelFormMain.Controls.Add(this.menuStripFormMaim, 0, 0);
            this.tableLayoutPanelFormMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelFormMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelFormMain.Name = "tableLayoutPanelFormMain";
            this.tableLayoutPanelFormMain.RowCount = 3;
            this.tableLayoutPanelFormMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelFormMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelFormMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelFormMain.Size = new System.Drawing.Size(1437, 613);
            this.tableLayoutPanelFormMain.TabIndex = 3;
            // 
            // toolStripFormMain
            // 
            this.toolStripFormMain.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStripFormMain.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripFormMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripFormMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelStatusSearch,
            toolStripSeparator6,
            this.buttonGetAllPhones,
            toolStripSeparator2,
            this.ComboBoxDepartment,
            toolStripSeparator3,
            this.ComboBoxPosition,
            toolStripSeparator4,
            this.buttonFilterCleaning,
            this.toolStripButtonApplyFilter,
            toolStripSeparator5,
            this.toolStripComboBoxCurrentColumn,
            toolStripSeparator1,
            this.labelRequestSearch});
            this.toolStripFormMain.Location = new System.Drawing.Point(0, 585);
            this.toolStripFormMain.Name = "toolStripFormMain";
            this.toolStripFormMain.Size = new System.Drawing.Size(1437, 28);
            this.toolStripFormMain.TabIndex = 7;
            this.toolStripFormMain.Text = "toolStrip1";
            // 
            // labelStatusSearch
            // 
            this.labelStatusSearch.Name = "labelStatusSearch";
            this.labelStatusSearch.Size = new System.Drawing.Size(52, 25);
            this.labelStatusSearch.Text = "ВЫКЛ";
            // 
            // buttonGetAllPhones
            // 
            this.buttonGetAllPhones.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonGetAllPhones.Image = ((System.Drawing.Image)(resources.GetObject("buttonGetAllPhones.Image")));
            this.buttonGetAllPhones.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonGetAllPhones.Name = "buttonGetAllPhones";
            this.buttonGetAllPhones.Size = new System.Drawing.Size(23, 25);
            this.buttonGetAllPhones.Text = "toolStripButton2";
            this.buttonGetAllPhones.Click += new System.EventHandler(this.buttonGetAllPhones_Click);
            // 
            // ComboBoxDepartment
            // 
            this.ComboBoxDepartment.AutoSize = false;
            this.ComboBoxDepartment.Name = "ComboBoxDepartment";
            this.ComboBoxDepartment.Size = new System.Drawing.Size(170, 23);
            // 
            // ComboBoxPosition
            // 
            this.ComboBoxPosition.AutoSize = false;
            this.ComboBoxPosition.Name = "ComboBoxPosition";
            this.ComboBoxPosition.Size = new System.Drawing.Size(170, 23);
            // 
            // buttonFilterCleaning
            // 
            this.buttonFilterCleaning.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonFilterCleaning.Image = ((System.Drawing.Image)(resources.GetObject("buttonFilterCleaning.Image")));
            this.buttonFilterCleaning.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonFilterCleaning.Name = "buttonFilterCleaning";
            this.buttonFilterCleaning.Size = new System.Drawing.Size(23, 25);
            this.buttonFilterCleaning.Text = "toolStripButton2";
            this.buttonFilterCleaning.Click += new System.EventHandler(this.buttonFilterCleaning_Click);
            // 
            // toolStripButtonApplyFilter
            // 
            this.toolStripButtonApplyFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonApplyFilter.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonApplyFilter.Image")));
            this.toolStripButtonApplyFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonApplyFilter.Name = "toolStripButtonApplyFilter";
            this.toolStripButtonApplyFilter.Size = new System.Drawing.Size(95, 25);
            this.toolStripButtonApplyFilter.Text = "Применить";
            this.toolStripButtonApplyFilter.Click += new System.EventHandler(this.toolStripButtonApplyFilter_Click);
            // 
            // toolStripComboBoxCurrentColumn
            // 
            this.toolStripComboBoxCurrentColumn.AutoSize = false;
            this.toolStripComboBoxCurrentColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxCurrentColumn.Items.AddRange(new object[] {
            "ФИО",
            "Внутренний номер",
            "Корпоративный номер",
            "Должность",
            "Подразделение"});
            this.toolStripComboBoxCurrentColumn.Name = "toolStripComboBoxCurrentColumn";
            this.toolStripComboBoxCurrentColumn.Size = new System.Drawing.Size(155, 23);
            this.toolStripComboBoxCurrentColumn.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxCurrentColumn_SelectedIndexChanged);
            // 
            // labelRequestSearch
            // 
            this.labelRequestSearch.Name = "labelRequestSearch";
            this.labelRequestSearch.Size = new System.Drawing.Size(61, 25);
            this.labelRequestSearch.Text = "Запрос";
            // 
            // commandsToolStripMenuItem
            // 
            this.commandsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemsOnTheEditToolStripMenuItem});
            this.commandsToolStripMenuItem.Name = "commandsToolStripMenuItem";
            this.commandsToolStripMenuItem.Size = new System.Drawing.Size(88, 26);
            this.commandsToolStripMenuItem.Text = "Команды";
            // 
            // itemsOnTheEditToolStripMenuItem
            // 
            this.itemsOnTheEditToolStripMenuItem.CheckOnClick = true;
            this.itemsOnTheEditToolStripMenuItem.Name = "itemsOnTheEditToolStripMenuItem";
            this.itemsOnTheEditToolStripMenuItem.Size = new System.Drawing.Size(275, 26);
            this.itemsOnTheEditToolStripMenuItem.Text = "Записи на редактировании";
            this.itemsOnTheEditToolStripMenuItem.Click += new System.EventHandler(this.itemsOnTheEditToolStripMenuItem_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1437, 613);
            this.Controls.Add(this.tableLayoutPanelFormMain);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MainMenuStrip = this.menuStripFormMaim;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormMain";
            this.Text = "Редактор записей";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPhones)).EndInit();
            this.contextMenuStripCommand.ResumeLayout(false);
            this.menuStripFormMaim.ResumeLayout(false);
            this.menuStripFormMaim.PerformLayout();
            this.tableLayoutPanelFormMain.ResumeLayout(false);
            this.tableLayoutPanelFormMain.PerformLayout();
            this.toolStripFormMain.ResumeLayout(false);
            this.toolStripFormMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView dataGridViewPhones;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripCommand;
        private System.Windows.Forms.ToolStripMenuItem insertNewPhoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem takeForEditingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem returnToTheArchiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editPhoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deletePhoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getAllPhonesToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStripFormMaim;
        private System.Windows.Forms.ToolStripMenuItem directoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem positionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem departmentsToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelFormMain;
        private System.Windows.Forms.ToolStrip toolStripFormMain;
        public System.Windows.Forms.ToolStripLabel labelStatusSearch;
        private System.Windows.Forms.ToolStripButton buttonGetAllPhones;
        public System.Windows.Forms.ToolStripComboBox ComboBoxDepartment;
        public System.Windows.Forms.ToolStripComboBox ComboBoxPosition;
        private System.Windows.Forms.ToolStripButton buttonFilterCleaning;
        private System.Windows.Forms.ToolStripButton toolStripButtonApplyFilter;
        public System.Windows.Forms.ToolStripComboBox toolStripComboBoxCurrentColumn;
        public System.Windows.Forms.ToolStripLabel labelRequestSearch;
        private System.Windows.Forms.ToolStripMenuItem commandsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemsOnTheEditToolStripMenuItem;
    }
}