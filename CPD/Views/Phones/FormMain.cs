﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPD.Controllers.Phones;

namespace CPD.Views.Phones
{
    public partial class FormMain : Form
    {
        private ControllerFormMain controller;

        public FormMain()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormMain(this);
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            controller.UpdateDataGridViewPhones();
        }

        private void getAllPhonesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.UpdateDataGridViewPhones();
        }

        private void deletePhoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.DeletingTheSelectedPhone();
        }

        private void takeForEditingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.IsEditing();
        }

        private void returnToTheArchiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.DeleteEditing();
        }

        private void insertNewPhoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.OpenFormInsert();
        }

        private void editPhoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.OpenFormUpdate();
        }

        private void positionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.OpenFormPositions();
        }

        private void departmentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.OpenFormDepartments();
        }

        private void buttonGetAllPhones_Click(object sender, EventArgs e)
        {
            controller.UpdateDataGridViewPhones();
        }

        private void buttonFilterCleaning_Click(object sender, EventArgs e)
        {
            controller.ResetTheFilter();
        }

        private void toolStripButtonApplyFilter_Click(object sender, EventArgs e)
        {
            controller.Filter();
        }

        private void dataGridViewPhones_KeyPress(object sender, KeyPressEventArgs e)
        {
            controller.SmartSearch(e);
        }

        private void toolStripComboBoxCurrentColumn_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridViewPhones.Focus();
        }

        private void itemsOnTheEditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.ItemsOnTheEdit(sender);
        }
    }
}
