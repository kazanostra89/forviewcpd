﻿namespace CPD.Views.Phones
{
    partial class FormUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUpdate));
            this.statusStripFormUpdate = new System.Windows.Forms.StatusStrip();
            this.statusUpdateDB = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripFormUpdate = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCancel = new System.Windows.Forms.ToolStripButton();
            this.checkBoxNumberMobile = new System.Windows.Forms.CheckBox();
            this.checkBoxNumberCity = new System.Windows.Forms.CheckBox();
            this.maskedTextBoxNumberCity = new System.Windows.Forms.MaskedTextBox();
            this.comboBoxPositions = new System.Windows.Forms.ComboBox();
            this.comboBoxDepartments = new System.Windows.Forms.ComboBox();
            this.maskedTextBoxNumberMobile = new System.Windows.Forms.MaskedTextBox();
            this.textBoxFullName = new System.Windows.Forms.TextBox();
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            label5 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            this.statusStripFormUpdate.SuspendLayout();
            this.toolStripFormUpdate.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(8, 208);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(99, 20);
            label5.TabIndex = 37;
            label5.Text = "Должность:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(8, 136);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(137, 20);
            label4.TabIndex = 36;
            label4.Text = "Подразделение:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(232, 288);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(184, 20);
            label3.TabIndex = 32;
            label3.Text = "Корпоративный номер:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(8, 288);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(154, 20);
            label2.TabIndex = 31;
            label2.Text = "Внутренний номер:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(8, 64);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(149, 20);
            label1.TabIndex = 30;
            label1.Text = "ФИО (полностью):";
            // 
            // statusStripFormUpdate
            // 
            this.statusStripFormUpdate.AutoSize = false;
            this.statusStripFormUpdate.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStripFormUpdate.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusUpdateDB});
            this.statusStripFormUpdate.Location = new System.Drawing.Point(0, 28);
            this.statusStripFormUpdate.Name = "statusStripFormUpdate";
            this.statusStripFormUpdate.Size = new System.Drawing.Size(584, 22);
            this.statusStripFormUpdate.SizingGrip = false;
            this.statusStripFormUpdate.TabIndex = 13;
            this.statusStripFormUpdate.Text = "statusStrip1";
            // 
            // statusUpdateDB
            // 
            this.statusUpdateDB.ForeColor = System.Drawing.Color.Blue;
            this.statusUpdateDB.Name = "statusUpdateDB";
            this.statusUpdateDB.Size = new System.Drawing.Size(100, 17);
            this.statusUpdateDB.Text = "Строк состояния";
            // 
            // toolStripFormUpdate
            // 
            this.toolStripFormUpdate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripFormUpdate.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripFormUpdate.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonUpdate,
            toolStripSeparator1,
            this.toolStripButtonCancel});
            this.toolStripFormUpdate.Location = new System.Drawing.Point(0, 0);
            this.toolStripFormUpdate.Name = "toolStripFormUpdate";
            this.toolStripFormUpdate.Size = new System.Drawing.Size(584, 28);
            this.toolStripFormUpdate.TabIndex = 12;
            this.toolStripFormUpdate.Text = "toolStrip1";
            // 
            // toolStripButtonUpdate
            // 
            this.toolStripButtonUpdate.AutoSize = false;
            this.toolStripButtonUpdate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonUpdate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUpdate.Image")));
            this.toolStripButtonUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUpdate.Name = "toolStripButtonUpdate";
            this.toolStripButtonUpdate.Size = new System.Drawing.Size(280, 25);
            this.toolStripButtonUpdate.Text = "Изменить";
            this.toolStripButtonUpdate.Click += new System.EventHandler(this.toolStripButtonUpdate_Click);
            // 
            // toolStripButtonCancel
            // 
            this.toolStripButtonCancel.AutoSize = false;
            this.toolStripButtonCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonCancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCancel.Image")));
            this.toolStripButtonCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCancel.Name = "toolStripButtonCancel";
            this.toolStripButtonCancel.Size = new System.Drawing.Size(280, 25);
            this.toolStripButtonCancel.Text = "Отмена";
            this.toolStripButtonCancel.Click += new System.EventHandler(this.toolStripButtonCancel_Click);
            // 
            // checkBoxNumberMobile
            // 
            this.checkBoxNumberMobile.AutoSize = true;
            this.checkBoxNumberMobile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxNumberMobile.Location = new System.Drawing.Point(568, 296);
            this.checkBoxNumberMobile.Name = "checkBoxNumberMobile";
            this.checkBoxNumberMobile.Size = new System.Drawing.Size(12, 11);
            this.checkBoxNumberMobile.TabIndex = 40;
            this.checkBoxNumberMobile.TabStop = false;
            this.checkBoxNumberMobile.UseVisualStyleBackColor = true;
            this.checkBoxNumberMobile.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxNumberCity
            // 
            this.checkBoxNumberCity.AutoSize = true;
            this.checkBoxNumberCity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxNumberCity.Location = new System.Drawing.Point(216, 296);
            this.checkBoxNumberCity.Name = "checkBoxNumberCity";
            this.checkBoxNumberCity.Size = new System.Drawing.Size(12, 11);
            this.checkBoxNumberCity.TabIndex = 39;
            this.checkBoxNumberCity.TabStop = false;
            this.checkBoxNumberCity.UseVisualStyleBackColor = true;
            this.checkBoxNumberCity.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // maskedTextBoxNumberCity
            // 
            this.maskedTextBoxNumberCity.Enabled = false;
            this.maskedTextBoxNumberCity.Location = new System.Drawing.Point(160, 288);
            this.maskedTextBoxNumberCity.Mask = "00-00";
            this.maskedTextBoxNumberCity.Name = "maskedTextBoxNumberCity";
            this.maskedTextBoxNumberCity.Size = new System.Drawing.Size(48, 26);
            this.maskedTextBoxNumberCity.TabIndex = 3;
            // 
            // comboBoxPositions
            // 
            this.comboBoxPositions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPositions.FormattingEnabled = true;
            this.comboBoxPositions.Location = new System.Drawing.Point(8, 240);
            this.comboBoxPositions.Name = "comboBoxPositions";
            this.comboBoxPositions.Size = new System.Drawing.Size(570, 28);
            this.comboBoxPositions.TabIndex = 2;
            // 
            // comboBoxDepartments
            // 
            this.comboBoxDepartments.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepartments.FormattingEnabled = true;
            this.comboBoxDepartments.Location = new System.Drawing.Point(8, 168);
            this.comboBoxDepartments.Name = "comboBoxDepartments";
            this.comboBoxDepartments.Size = new System.Drawing.Size(570, 28);
            this.comboBoxDepartments.TabIndex = 1;
            // 
            // maskedTextBoxNumberMobile
            // 
            this.maskedTextBoxNumberMobile.Enabled = false;
            this.maskedTextBoxNumberMobile.Location = new System.Drawing.Point(424, 288);
            this.maskedTextBoxNumberMobile.Mask = "+7(999)000-00-00";
            this.maskedTextBoxNumberMobile.Name = "maskedTextBoxNumberMobile";
            this.maskedTextBoxNumberMobile.Size = new System.Drawing.Size(136, 26);
            this.maskedTextBoxNumberMobile.TabIndex = 4;
            // 
            // textBoxFullName
            // 
            this.textBoxFullName.Location = new System.Drawing.Point(8, 96);
            this.textBoxFullName.Name = "textBoxFullName";
            this.textBoxFullName.Size = new System.Drawing.Size(570, 26);
            this.textBoxFullName.TabIndex = 0;
            this.textBoxFullName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxName_KeyPress);
            // 
            // FormUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 332);
            this.Controls.Add(this.checkBoxNumberMobile);
            this.Controls.Add(this.checkBoxNumberCity);
            this.Controls.Add(this.maskedTextBoxNumberCity);
            this.Controls.Add(label5);
            this.Controls.Add(label4);
            this.Controls.Add(this.comboBoxPositions);
            this.Controls.Add(this.comboBoxDepartments);
            this.Controls.Add(this.maskedTextBoxNumberMobile);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.textBoxFullName);
            this.Controls.Add(this.statusStripFormUpdate);
            this.Controls.Add(this.toolStripFormUpdate);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактирование";
            this.Load += new System.EventHandler(this.FormUpdate_Load);
            this.statusStripFormUpdate.ResumeLayout(false);
            this.statusStripFormUpdate.PerformLayout();
            this.toolStripFormUpdate.ResumeLayout(false);
            this.toolStripFormUpdate.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStripFormUpdate;
        public System.Windows.Forms.ToolStripStatusLabel statusUpdateDB;
        private System.Windows.Forms.ToolStrip toolStripFormUpdate;
        public System.Windows.Forms.ToolStripButton toolStripButtonUpdate;
        public System.Windows.Forms.ToolStripButton toolStripButtonCancel;
        public System.Windows.Forms.CheckBox checkBoxNumberMobile;
        public System.Windows.Forms.CheckBox checkBoxNumberCity;
        public System.Windows.Forms.MaskedTextBox maskedTextBoxNumberCity;
        public System.Windows.Forms.ComboBox comboBoxPositions;
        public System.Windows.Forms.ComboBox comboBoxDepartments;
        public System.Windows.Forms.MaskedTextBox maskedTextBoxNumberMobile;
        public System.Windows.Forms.TextBox textBoxFullName;
    }
}